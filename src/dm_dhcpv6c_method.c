/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_dhcpv6client.h"
#include "dm_client.h"
#include "dm_server.h"
#include "firewall.h"

/*
   example
   args = {
      mod_name = "mod-odhcp6c",
      fnc = "odhcp6c-script",
      data = {
          action = "started",
          phys_interf = "eth0"
      }
   }
   returns -1 called function signals an error
           -2 unknown function, module name or empty data parameter
 */
amxd_status_t _update(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    amxc_var_t* data = NULL;
    const char* mod_name = NULL;
    const char* func_name = NULL;
    int retval = -1;

    mod_name = GET_CHAR(args, "mod_name");
    func_name = GET_CHAR(args, "fnc");
    data = amxc_var_get_path(args, "data", AMXC_VAR_FLAG_DEFAULT);

    SAH_TRACEZ_INFO(ME, "Mod %s, func '%s', data %sok",
                    mod_name, func_name,
                    (data == NULL) ? "not " : "");
    when_str_empty(mod_name, exit);
    when_str_empty(func_name, exit);
    when_null(data, exit);

    retval = amxm_execute_function(mod_name, mod_name, func_name, data, ret);
exit:
    if(retval != 0) {
        // Function should be implemented
        SAH_TRACEZ_ERROR(ME, "Mod %s, func '%s' returned %d",
                         mod_name, func_name, retval);
        amxc_var_set(int32_t, ret, -2);
    }
    return amxd_status_ok;
}

/*
   example
   arg = {
      Interface = "IP.Interface.2."
   }
 */
int dm_client_stopped(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    int retval = -1;
    const char* interface = NULL;
    amxd_object_t* dm_dhcp_client = NULL;

    interface = GET_CHAR(args, "Interface");
    when_str_empty(interface, exit);

    dm_dhcp_client = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.[Interface=='%s']",
                                   interface);
    when_null(dm_dhcp_client, exit);

    dm_client_init(dm_dhcp_client);
    retval = 0;

exit:
    amxc_var_set(int32_t, ret, retval);

    return retval;
}

/*
   example
   arg = {
      Interface = "IP.Interface.2."
   }
 */
int dm_client_started(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    int retval = -1;
    const char* interface = GET_CHAR(args, "Interface");
    amxd_object_t* dm_dhcp_client =
        amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.[Interface=='%s']", interface);

    when_null_trace(dm_dhcp_client, exit, ERROR, "Object \'DHCPv6.Client.\' " \
                    "for %s not found", interface);
    dm_client_status(dm_dhcp_client, dhcpv6c_status_error_misconfigure);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);

    return retval;
}

/*
   example
   arg = {
      Interface = "IP.Interface.2."
   }
 */
int dm_client_misconfigured(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    int retval = -1;
    const char* interface = GET_CHAR(args, "Interface");
    amxd_object_t* dm_dhcp_client =
        amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.[Interface=='%s']", interface);
    when_null_trace(dm_dhcp_client, exit, ERROR, "Object \'DHCPv6.Client.\' " \
                    "for %s not found", interface);

    dm_client_status(dm_dhcp_client, dhcpv6c_status_error_misconfigure);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);

    return retval;
}

amxd_status_t _Renew(amxd_object_t* object,
                     UNUSED amxd_function_t* func,
                     UNUSED amxc_var_t* args,
                     UNUSED amxc_var_t* ret) {
    client_info_t* info = (client_info_t*) object->priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(info, exit, ERROR, "No internal data found for the client");
    status = amxd_status_ok;
    when_false(info->flags.bit.enable || info->flags.bit.req_addr || info->flags.bit.req_pref, exit);
    dm_dhcpv6c_renew_client(info);
exit:
    return status;

}

amxd_status_t _Release(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       UNUSED amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    client_info_t* info = (client_info_t*) object->priv;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(info, exit, ERROR, "No internal data found for the client");
    status = amxd_status_ok;
    when_false(info->flags.bit.enable || info->flags.bit.req_addr || info->flags.bit.req_pref, exit);
    info->flags.bit.force_stop = 1;
    dm_client_status(object, info->flags.bit.enable ? dhcpv6c_status_error_misconfigure : dhcpv6c_status_disabled);
    dm_dhcpv6c_start_stop_client(info);
    info->flags.bit.force_stop = 0;
    dm_dhcpv6c_start_stop_client(info);
exit:
    return status;
}

