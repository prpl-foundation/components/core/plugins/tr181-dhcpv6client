/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_client.h"
#include "dm_options.h"

#define UNKNOWN_TIME "0001-01-01T00:00:00Z"

/*
   Contents of parameter:
   args = {
      Server = "DHCPv6.Client.1.Server.1.",
      ReceivedOption [
        { Tag = "1", Value = "00030001021018010c01" },
        ...
      ]
   }
 */
int dm_options_add(UNUSED const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret) {
    int retval = -1;
    amxd_trans_t trans;
    amxd_object_t* option = NULL;
    amxd_object_t* client = NULL;
    const char* server = GET_CHAR(args, "Server");
    char* client_path = NULL;

    when_str_empty(server, exit);
    client_path = strdup(server);
    when_str_empty(client_path, exit);
    // server = "DHCPv6Client.Client.1.Server.1." - > "DHCPv6Client.Client.1."
    client_path[22] = '\0';
    option = amxd_dm_findf(dhcpv6c_get_dm(), "%s.ReceivedOption.", client_path);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_var_for_each(var, GET_ARG(args, "ReceivedOption")) {
        amxd_object_t* option_obj = amxd_object_findf(option, ".[Tag == '%u' && Server == '%s'].", GET_UINT32(var, "Tag"), server);
        amxc_ts_t ts;
        amxc_ts_now(&ts);
        amxd_trans_select_object(&trans, option);
        if(option_obj == NULL) {
            amxd_trans_add_inst(&trans, 0, NULL);
            amxd_trans_set_value(cstring_t, &trans, "Server", server);
        } else {
            amxd_trans_select_object(&trans, option_obj);
        }
        amxd_trans_set_value(uint32_t, &trans, "Tag", GET_UINT32(var, "Tag"));
        amxd_trans_set_value(cstring_t, &trans, "Value", GET_CHAR(var, "Value"));
        amxd_trans_set_value(amxc_ts_t, &trans, "LastUpdate", &ts);
    }
    retval = amxd_trans_apply(&trans, dhcpv6c_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(retval, exit, ERROR, "failed to add ReceivedOption " \
                      "(%d)", retval);
    client = amxd_dm_findf(dhcpv6c_get_dm(), "%s", client_path);
    when_null(client, exit);
    dm_client_status(client, dhcpv6c_status_enabled);

exit:
    amxc_var_set(int32_t, ret, retval);
    free(client_path);
    return retval;
}

int dm_options_remove(const char* server,
                      amxd_object_t* client) {
    amxc_llist_t options;
    amxc_llist_init(&options);

    when_null(client, exit);
    amxd_object_resolve_pathf(client, &options,
                              ".ReceivedOption.[Server=='%s']",
                              server);
    amxc_llist_for_each(it, (&options)) {
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxd_object_t* obj = amxd_dm_findf(dhcpv6c_get_dm(), "%s", path);
        amxd_object_emit_del_inst(obj);
        amxd_object_delete(&obj);
    }
exit:
    amxc_llist_clean(&options, amxc_string_list_it_free);
    return 0;
}

int dm_config_timing_set(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_trans_t trans;
    amxd_object_t* client = dm_get_client(args);
    uint32_t action = GET_UINT32(args, "Action");
    const char* prefix = GET_CHAR(amxo_parser_get_config(dhcpv6c_get_parser(), "prefix_"), NULL);
    amxd_object_t* config_obj = NULL;
    char* object_path = NULL;
    int status = 1;
    amxc_ts_t time;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_ts_now(&time);

    when_null_trace(prefix, exit, ERROR, "No vendor prefix found");
    when_null_trace(client, exit, ERROR, "No client found");

    object_path = amxd_object_get_path(client, AMXD_OBJECT_TERMINATE);
    when_str_empty_trace(object_path, exit, ERROR, "No path given for the client");

    config_obj = amxd_dm_findf(dhcpv6c_get_dm(), "%s%sConfig.", object_path, prefix);
    when_null_trace(config_obj, exit, ERROR, "No config object found for server %s", object_path);

    amxd_trans_select_object(&trans, config_obj);

    switch(action) {
    case DHCPV6_ACTION_UPDATED:
        amxd_trans_set_bool(&trans, "T1Renewed", true);
        amxd_trans_set_amxc_ts_t(&trans, "LeaseRenewedWhen", &time);
        break;
    case DHCPV6_ACTION_REBOUND:
        amxd_trans_set_bool(&trans, "T2Renewed", true);
        break;
    case DHCPV6_ACTION_TIMING:
        amxd_trans_set_amxc_ts_t(&trans, "LastT1SentWhen", &time);
        amxd_trans_set_amxc_ts_t(&trans, "LastT2SentWhen", &time);
        break;
    case DHCPV6_ACTION_STOP:
        amxc_ts_parse(&time, UNKNOWN_TIME, strlen(UNKNOWN_TIME));
        amxd_trans_set_amxc_ts_t(&trans, "LastT1SentWhen", &time);
        amxd_trans_set_amxc_ts_t(&trans, "LastT2SentWhen", &time);
        amxd_trans_set_amxc_ts_t(&trans, "LeaseRenewedWhen", &time);
        amxd_trans_set_bool(&trans, "T1Renewed", false);
        amxd_trans_set_bool(&trans, "T2Renewed", false);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "DHCPv6 action not found");
        break;
    }

    status = amxd_trans_apply(&trans, dhcpv6c_get_dm());
    when_failed_trace(status, exit, ERROR, "Unable to update Config of dhcpv6 client %s%sConfig. : %d", amxd_object_get_path(client, AMXD_OBJECT_TERMINATE), prefix, status);
exit:
    free(object_path);
    amxd_trans_clean(&trans);
    return status;
}
