/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dm_dhcpv6client.h"
#include "client_info.h"
#include "dm_client.h"
#include "firewall.h"

static void dm_dhcpv6c_add_sent_options(amxd_object_t* client,
                                        amxc_var_t* data) {

    amxc_var_t* var_opts = amxc_var_add_key(amxc_llist_t, data, "SentOption", NULL);
    amxc_llist_t options;
    amxc_llist_init(&options);

    amxd_object_resolve_pathf(client, &options, ".SentOption.*");
    amxc_llist_for_each(it, (&options)) {
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        amxd_object_t* obj = amxd_dm_findf(dhcpv6c_get_dm(), "%s", path);
        if(amxd_object_get_value(bool, obj, "Enable", NULL)) {
            amxc_var_t* option = amxc_var_add(amxc_htable_t, var_opts, NULL);
            char* value = amxd_object_get_value(cstring_t, obj, "Value", NULL);
            amxc_var_add_key(cstring_t, option, "Value", value);
            amxc_var_add_key(uint16_t, option, "Tag",
                             amxd_object_get_value(uint16_t, obj, "Tag", NULL));
            free(value);
        }
    }
    amxc_llist_clean(&options, amxc_string_list_it_free);
}

/**
 * @brief
 * Check condition to start or stop the back-end client if needed
 *
 * @param info the private client information
 */
void dm_dhcpv6c_start_stop_client(client_info_t* info) {
    bool should_start = true;
    const char* module = NULL;
    int retval = -1;
    amxc_var_t result;
    amxc_var_t data;
    amxd_object_t* retransmission_obj = NULL;
    uint32_t sol_max_rt = 0;
    uint32_t dscp = 0;

    amxc_var_init(&result);
    amxc_var_init(&data);

    when_null_trace(info, exit, ERROR, "Cannot get private data from client object");
    when_null_trace(info->obj, exit, ERROR, "Cannot get client object");

    module = GET_CHAR(amxd_object_get_param_value(info->obj, "Controller"), NULL);
    when_str_empty_trace(module, exit, ERROR, "Cannot get client controller");

    dscp = amxd_object_get_value(uint32_t, info->obj, "DSCP", NULL);

    retransmission_obj = amxd_object_get_child(info->obj, "Retransmission.");
    when_null_trace(retransmission_obj, exit, ERROR, "Cannot get the Retransmission Object of the %s",
                    amxd_object_get_name(info->obj, AMXD_OBJECT_NAMED));
    sol_max_rt = amxd_object_get_value(uint32_t, retransmission_obj, "SolicitMaxTimeout", NULL);

    // check that we have enough information to start the client
    should_start = info->flags.bit.enable & info->flags.bit.intf_exist & info->flags.bit.mac_ok & info->ra_info.flags.bit.up_flag;

    // force to send a release message by shutting down the back-end
    should_start &= !info->flags.bit.force_stop;

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", info->intf_path);
    amxc_var_add_key(cstring_t, &data, "ifname", info->intf_name);
    amxc_var_add_key(bool, &data, "RequestAddresses", info->flags.bit.req_addr & info->ra_info.flags.bit.m_flag);
    amxc_var_add_key(bool, &data, "RequestPrefixes", info->flags.bit.req_pref & (info->ra_info.flags.bit.m_flag | info->ra_info.flags.bit.o_flag));
    amxc_var_add_key(uint32_t, &data, "sol_max_rt", sol_max_rt);
    amxc_var_add_key(uint32_t, &data, "dscp", dscp);
    if(should_start) {
        const char* reqopts = GET_CHAR(amxd_object_get_param_value(info->obj, "RequestedOptions"), NULL);
        amxc_var_add_key(cstring_t, &data, "RequestedOptions", reqopts);
        dm_dhcpv6c_add_sent_options(info->obj, &data);
        retval = amxm_execute_function(module, module, "start-dhcpv6c", &data, &result);
        info->flags.bit.client_started = 1;
    } else if(info->flags.bit.client_started) {
        retval = amxm_execute_function(module, module, "stop-dhcpv6c", &data, &result);
        info->flags.bit.client_started = 0;
    } else {
        retval = 0;
    }

    if(retval != 0) {
        // if module exists, function should be implemented
        SAH_TRACEZ_ERROR(ME, "Mod %s, start '%d', returned %d",
                         module, should_start, retval);
    }

    if(info->flags.bit.enable && !info->flags.bit.firewall_up) {
        firewall_open_dhcp_port(info->intf_path, amxd_object_get_name(info->obj, AMXD_OBJECT_NAMED));
        info->flags.bit.firewall_up = 1;
    } else if(!info->flags.bit.enable && info->flags.bit.firewall_up) {
        firewall_close_dhcp_port(info->intf_path, amxd_object_get_name(info->obj, AMXD_OBJECT_NAMED));
        info->flags.bit.firewall_up = 0;
    }

    dm_client_set_ia_status(info->obj, &data);
exit:
    amxc_var_clean(&result);
    amxc_var_clean(&data);
}

/**
 * @brief
 * Check condition to start or stop the back-end client if needed
 *
 * @param info the private client information
 */
void dm_dhcpv6c_renew_client(client_info_t* info) {
    const char* module = NULL;
    int retval = -1;
    amxc_var_t result;
    amxc_var_t data;

    amxc_var_init(&result);
    amxc_var_init(&data);

    when_null_trace(info, exit, ERROR, "Cannot get private data from client object");
    when_null_trace(info->obj, exit, ERROR, "Cannot get client object");

    module = GET_CHAR(amxd_object_get_param_value(info->obj, "Controller"), NULL);
    when_str_empty_trace(module, exit, ERROR, "Cannot get client controller");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", info->intf_path);
    amxc_var_add_key(cstring_t, &data, "ifname", info->intf_name);

    if(info->flags.bit.client_started) {
        retval = amxm_execute_function(module, module, "renew-dhcpv6c", &data, &result);
    }

    if(retval != 0) {
        // if module exists, function should be implemented
        SAH_TRACEZ_ERROR(ME, "Mod %s, renew '%d', returned %d",
                         module, info->flags.bit.client_started, retval);
    }
exit:
    amxc_var_clean(&result);
    amxc_var_clean(&data);
}