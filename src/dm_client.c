/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_client.h"

const char* str_status[] = {
    "Disabled",
    "Enabled",
    "Error_Misconfigured"
};

void dm_client_status(amxd_object_t* client, dhcpv6c_status_t status) {
    amxd_trans_t trans;
    amxd_status_t retval = amxd_status_unknown_error;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    when_false(status < dhcpv6c_status_last, exit);

    amxd_trans_select_object(&trans, client);
    amxd_trans_set_value(cstring_t, &trans, "Status", str_status[status]);
    retval = amxd_trans_apply(&trans, dhcpv6c_get_dm());
    (void) retval; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "amxd_trans_apply (%d) %s", retval, str_status[status]);
exit:
    amxd_trans_clean(&trans);
}

void dm_client_set_ia_status(amxd_object_t* client, amxc_var_t* data) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    when_null_trace(client, exit, ERROR, "Client object is NULL");
    when_null_trace(data, exit, ERROR, "Data variant is NULL");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, client);
    amxd_trans_set_value(cstring_t, &trans, "RequestAddressesStatus", GET_BOOL(data, "RequestAddresses") ? "Enabled" : "Disabled");
    amxd_trans_set_value(cstring_t, &trans, "RequestPrefixesStatus", GET_BOOL(data, "RequestPrefixes") ? "Enabled" : "Disabled");

    when_failed_trace(amxd_trans_apply(&trans, dhcpv6c_get_dm()), exit, ERROR, "Transaction failed");

exit:
    amxd_trans_clean(&trans);
}

amxd_object_t* dm_get_client(amxc_var_t* args) {
    amxd_object_t* dm_client = NULL;
    const char* interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "Interface args is null");
    dm_client = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.[Interface=='%s']",
                              interface);
exit:
    return dm_client;
}

static int dm_client_remove_children_cb(UNUSED amxd_object_t* templ,
                                        amxd_object_t* child,
                                        UNUSED void* priv) {
    amxd_object_emit_del_inst(child);
    amxd_object_delete(&child);
    return 0;
}

/**
 * @brief
 * Reinitialise the DM of a client
 *
 * @param client The client object to reinitialise the DM
 * @return int
 */
void dm_client_init(amxd_object_t* client) {
    amxd_trans_t trans;
    dhcpv6c_status_t status = dhcpv6c_status_error_misconfigure;
    int retval = -1;

    amxd_trans_init(&trans);
    when_null_trace(client, exit, ERROR, "DM Client is null");
    amxd_object_for_all(client, ".Server.*", dm_client_remove_children_cb, NULL);
    amxd_object_for_all(client, ".ReceivedOption.*", dm_client_remove_children_cb, NULL);
    if(!amxd_object_get_value(bool, client, "Enable", NULL)) {
        status = dhcpv6c_status_disabled;
    }
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, client);
    amxd_trans_set_value(cstring_t, &trans, "Status", str_status[status]);

    retval = amxd_trans_apply(&trans, dhcpv6c_get_dm());
    when_failed_trace(retval, exit, ERROR, "Unable to clean client DM %d", retval);
exit:
    amxd_trans_clean(&trans);
}
