/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "client_info.h"
#include "dm_dhcpv6client.h"
#include "dm_client_duid.h"

#define ME "dhcpv6c"

#define str_empty(str) (((str) == NULL) || (*(str) == 0))

/**
 * @brief
 * Handle the timer when information are dependent of the down state of the interface
 * The flag in the timer flags of the parameter triggering the timer need to be set/clear before calling the function
 *
 * @param info The client private information containing the timer flags
 * @param start If the timer need to be started or stopped
 * @return true If the shutdown of the client back-end will be handle by the timer
 */
static bool client_handle_timer(client_info_t* info, bool start) {
    int32_t time = amxd_object_get_value(int32_t, info->obj, "ResetOnPhysDownTimeout", NULL);
    bool ret = true;

    when_null_status(info, exit, ret = false);

    // if interface goes from up to down, handle timer
    if(start && ((amxp_timer_get_state(info->timer_physdown) != amxp_timer_started) || (amxp_timer_get_state(info->timer_physdown) != amxp_timer_running))) {
        if(time > 0) {
            SAH_TRACEZ_INFO(ME, "Timer started");
            amxp_timer_start(info->timer_physdown, time);
        }
        // directly shutdown if timer == 0
        if(time == 0) {
            SAH_TRACEZ_INFO(ME, "Directly stopped");
            ret = false;
        }
        // otherwise, never shutdown
    }
    if(!start && (info->timer_flags.all_flags == 0)) {
        amxp_timer_stop(info->timer_physdown);
    }
exit:
    return ret;
}

static void client_isup_netmodel_cb(UNUSED const char* sig_name,
                                    const amxc_var_t* data,
                                    void* priv) {
    client_info_t* info = (client_info_t*) priv;
    const char* intf_name = NULL;

    when_null(data, exit);
    when_null(info, exit);
    when_null(info->obj, exit);

    intf_name = amxc_var_constcast(cstring_t, data);

    SAH_TRACEZ_INFO(ME, "intf_path %s: intf_name %s -> %s",
                    info->intf_path, info->intf_name, intf_name);

    if(str_empty(intf_name)) {
        info->timer_flags.bit.r_intf = 1;
        when_true(client_handle_timer(info, true), exit);
    } else {
        info->timer_flags.bit.r_intf = 0;
        client_handle_timer(info, false);
        if(info->intf_name != NULL) {
            free(info->intf_name);
        }
        info->intf_name = strdup(intf_name);
        info->flags.bit.intf_exist = 1;
    }

    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

static void client_mac_netmodel_cb(UNUSED const char* sig_name,
                                   const amxc_var_t* data,
                                   void* priv) {
    client_info_t* info = (client_info_t*) priv;
    const char* mac_addr = NULL;

    when_null(data, exit);
    when_null(info, exit);
    when_null(info->obj, exit);
    mac_addr = amxc_var_constcast(cstring_t, data);
    SAH_TRACEZ_INFO(ME, "intf_path %s: mac addr %s -> %s",
                    info->intf_path, info->mac_addr, mac_addr);

    if(info->mac_addr != NULL) {
        free(info->mac_addr);
        info->mac_addr = NULL;
    }

    if(str_empty(mac_addr)) {
        amxd_object_set_value(cstring_t, info->obj, "DUID", "");
        info->flags.bit.mac_ok = 0;
    } else {
        info->mac_addr = strdup(mac_addr);
        info->flags.bit.mac_ok = 1;
        dm_client_duid_set(info->obj, mac_addr);
    }
    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

static void client_ra_up_netmodel_cb(UNUSED const char* sig_name,
                                     const amxc_var_t* data,
                                     void* priv) {
    client_info_t* info = (client_info_t*) priv;
    bool flag_up = GET_BOOL(data, NULL);
    when_null_trace(info, exit, ERROR, "DHCPv6Client private info struct is NULL");
    when_null_trace(info->obj, exit, ERROR, "Private info does not have a valid client object");

    if(info->ra_info.flags.bit.up_flag & !flag_up) {
        info->timer_flags.bit.r_ra_up = 1;
        client_handle_timer(info, true);
    } else {
        info->timer_flags.bit.r_ra_up = 0;
        client_handle_timer(info, false);
        info->ra_info.flags.bit.up_flag = flag_up;
    }
    SAH_TRACEZ_INFO(ME, "Received iprouter-up flag value of %u", flag_up);

    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

static void client_managed_flag_netmodel_cb(UNUSED const char* sig_name,
                                            const amxc_var_t* data,
                                            void* priv) {
    client_info_t* info = (client_info_t*) priv;
    when_null_trace(info, exit, ERROR, "DHCPv6Client private info struct is NULL");
    when_null_trace(info->obj, exit, ERROR, "Private info does not have a valid client object");

    info->ra_info.flags.bit.m_flag = GET_BOOL(data, NULL);
    SAH_TRACEZ_INFO(ME, "Received Managed flag value of %u", info->ra_info.flags.bit.m_flag);

    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

static void client_other_flag_netmodel_cb(UNUSED const char* sig_name,
                                          const amxc_var_t* data,
                                          void* priv) {
    client_info_t* info = (client_info_t*) priv;
    when_null_trace(info, exit, ERROR, "DHCPv6Client private info struct is NULL");
    when_null_trace(info->obj, exit, ERROR, "Private info does not have a valid client object");

    info->ra_info.flags.bit.o_flag = GET_BOOL(data, NULL);
    SAH_TRACEZ_INFO(ME, "Received OtherConfig flag value of %u", info->ra_info.flags.bit.o_flag);

    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

/**
 * @brief
 * Start the configuration gather of the client to enable the back-end. This function open all the
 * NetModel queries needed
 *
 * @param info The private information of the client to enable in the back-end
 */
void client_info_enable(client_info_t* info) {
    char* intf_path = NULL;

    when_null_trace(info, exit, ERROR, "private data of client cannot be found");
    when_null_trace(info->obj, exit, ERROR, "No client object link to private data of a client object");

    intf_path = amxd_object_get_value(cstring_t, info->obj, "Interface", NULL);
    when_str_empty_trace(intf_path, exit, ERROR, "No interface path given");

    if(info->intf_path != NULL) {
        free(info->intf_path);
    }
    info->intf_path = intf_path;

    info->flags.bit.req_addr = amxd_object_get_value(bool, info->obj, "RequestAddresses", NULL);
    info->flags.bit.req_pref = amxd_object_get_value(bool, info->obj, "RequestPrefixes", NULL);

    if(info->q_mac != NULL) {
        netmodel_closeQuery(info->q_mac);
        info->q_mac = NULL;
        info->flags.bit.mac_ok = 0;
        free(info->mac_addr);
        info->mac_addr = NULL;
    }
    if(info->q_isup != NULL) {
        netmodel_closeQuery(info->q_isup);
        info->q_isup = NULL;
        info->flags.bit.intf_exist = 0;
        free(info->intf_name);
        info->intf_name = NULL;
    }

    if(info->ra_info.q_ra_up != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_up);
        info->ra_info.q_ra_up = NULL;
        info->ra_info.flags.bit.up_flag = 0;

    }
    if(info->ra_info.q_ra_managed_flag != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_managed_flag);
        info->ra_info.q_ra_managed_flag = NULL;
        info->ra_info.flags.bit.m_flag = 0;

    }
    if(info->ra_info.q_ra_other_flag != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_other_flag);
        info->ra_info.q_ra_other_flag = NULL;
        info->ra_info.flags.bit.o_flag = 0;
    }

    when_str_empty_trace(info->intf_path, exit, WARNING, "No path given for the interface, cannot start client");

    info->q_mac = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                       "tr181-dhcpv6client",
                                                       "MACAddress",
                                                       NULL,
                                                       netmodel_traverse_down,
                                                       client_mac_netmodel_cb, (void*) info);
    if(info->q_mac != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a MAC query to netmodel, for intf %s", info->intf_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel");
    }

    info->q_isup = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                        "tr181-dhcpv6client",
                                                        "NetDevName",
                                                        "ipv6 && netdev-up",
                                                        netmodel_traverse_down,
                                                        client_isup_netmodel_cb, (void*) info);
    if(info->q_isup != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel, for isup %s",
                        info->intf_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add isup query to netmodel");
    }

    // ra flag queries

    info->ra_info.q_ra_up = netmodel_openQuery_hasFlag((const char*) info->intf_path,
                                                       "tr181-dhcpv6client",
                                                       "iprouter-up", "",
                                                       netmodel_traverse_down,
                                                       client_ra_up_netmodel_cb, (void*) info);

    if(info->ra_info.q_ra_up != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel for iprouter-up %s",
                        info->intf_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add ra_up query to netmodel");
    }


    info->ra_info.q_ra_managed_flag = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                                           "tr181-dhcpv6client",
                                                                           "IPv6Router.Managed", NULL,
                                                                           netmodel_traverse_down,
                                                                           client_managed_flag_netmodel_cb, (void*) info);

    if(info->ra_info.q_ra_managed_flag != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel for IPv6Router.Managed %s",
                        info->intf_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add managed_ra query to netmodel");
    }

    info->ra_info.q_ra_other_flag = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                                         "tr181-dhcpv6client",
                                                                         "IPv6Router.Other", NULL,
                                                                         netmodel_traverse_down,
                                                                         client_other_flag_netmodel_cb, (void*) info);
    if(info->ra_info.q_ra_other_flag != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel for IPv6Router.Other %s",
                        info->intf_path);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add ra_other query to netmodel");
    }
exit:
    return;
}

/**
 * @brief
 * Stop the client as soon as the timer expired as the interface is down for too long
 *
 * @param local_timer The timer handling the shutdown on phys down
 * @param priv The private data of the client object with the interface down
 */
void dhcpv6c_delay_stop_phys_down(UNUSED amxp_timer_t* local_timer,
                                  void* priv) {
    client_info_t* info = (client_info_t*) priv;
    when_null_trace(info, exit, ERROR, "No private data for delayed shutdown in client");

    if(info->timer_flags.bit.r_intf) {
        info->flags.bit.intf_exist = 0;
    }
    if(info->timer_flags.bit.r_ra_up) {
        info->ra_info.flags.bit.up_flag = 0;
    }

    dm_dhcpv6c_start_stop_client(info);
    if(info->timer_flags.bit.r_intf) {
        free(info->intf_name);
        info->intf_name = NULL;
    }
    info->timer_flags.all_flags = 0;
exit:
    return;
}

/**
 * @brief
 * Create private information for a dhcpv6 client, will enable the client if Enable is set to true
 *
 * @param obj The client instance object in the DM
 */
void client_info_create(amxd_object_t* obj) {
    client_info_t* info = NULL;

    when_null_trace(obj, exit, ERROR, "Object is NULL");
    when_false_trace(obj->priv == NULL, exit, ERROR, " priv was not null. For object: %s", obj->name);


    info = (client_info_t*) calloc(1, sizeof(client_info_t));
    obj->priv = info;
    info->obj = obj;
    info->mac_addr = NULL;
    info->intf_path = NULL;
    info->flags.all_flags = 0;
    info->timer_flags.all_flags = 0;

    amxp_timer_new(&(info->timer_physdown), dhcpv6c_delay_stop_phys_down, info);

    info->flags.bit.enable = amxd_object_get_value(bool, obj, "Enable", NULL);

    client_info_enable(info);
exit:
    return;
}

/**
 * @brief
 * Clean the information of a dhcpv6 client. This function only need to be called when the client will not be used anymore.
 * It will also stop the client in the back-end if not already done.
 *
 * @param info The private data of the client to clean.
 */
void client_info_clear(client_info_t* info) {
    when_null(info, exit);

    info->flags.bit.enable = 0;
    dm_dhcpv6c_start_stop_client(info);

    info->flags.all_flags = 0;
    info->timer_flags.all_flags = 0;
    info->ra_info.flags.all_flags = 0;

    dm_dhcpv6c_start_stop_client(info);

    amxp_timer_delete(&(info->timer_physdown));

    if(info->q_isup != NULL) {
        netmodel_closeQuery(info->q_isup);
    }
    if(info->q_mac != NULL) {
        netmodel_closeQuery(info->q_mac);
    }

    if(info->ra_info.q_ra_up != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_up);
    }
    if(info->ra_info.q_ra_managed_flag != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_managed_flag);
    }
    if(info->ra_info.q_ra_other_flag != NULL) {
        netmodel_closeQuery(info->ra_info.q_ra_other_flag);
    }

    if(info->obj != NULL) {
        info->obj->priv = NULL;
    }

    free(info->intf_name);
    free(info->intf_path);
    free(info->mac_addr);
    free(info);
exit:
    return;
}