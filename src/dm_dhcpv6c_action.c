/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_dhcpv6client.h"
#include "dm_client.h"
#include "client_info.h"

amxd_status_t _object_delete(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t rv = amxd_status_invalid_action;
    uint32_t instances = amxd_object_get_instance_count(object);

    when_false(reason == action_object_del_inst, exit);

    if(instances > 1) {
        rv = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_INFO(ME, "%d instance(s) of object %s, (reason %d), retval %d",
                    instances, object->name, reason, rv);

    return rv;
}

amxd_status_t _client_object_delete(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    if(object != NULL) {
        client_info_clear((client_info_t*) object->priv);
        object->priv = NULL;
    }
    return amxd_action_object_destroy(object, param, reason, args, retval, priv);
}

amxd_status_t _client_renew(amxd_object_t* object,
                            UNUSED amxd_param_t* param,
                            amxd_action_t reason,
                            const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            UNUSED void* priv) {
    int ret = 1;
    amxd_status_t status = amxd_status_unknown_error;
    bool renew_enabled = false;

    if(reason != action_param_write) {
        return amxd_status_function_not_implemented;
    }

    client_info_t* info = (client_info_t*) object->priv;
    when_null_trace(info, exit, ERROR, "No private data linked to the client");

    status = amxd_status_ok;
    renew_enabled = amxc_var_dyncast(bool, args);
    when_false(renew_enabled, exit);

    if(!info->flags.bit.enable || (!info->flags.bit.req_addr && !info->flags.bit.req_pref)) {
        // Do nothing if DHCPv6 client is disabled
        SAH_TRACEZ_INFO(ME, "func renew disabled");
    } else {
        dm_dhcpv6c_renew_client(info);
    }

exit:
    amxc_var_set(int32_t, retval, ret);
    return status;
}

amxd_status_t _client_release(amxd_object_t* object,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              UNUSED void* priv) {
    int ret = 1;
    amxd_status_t status = amxd_status_unknown_error;
    bool renew_enabled = false;

    if(reason != action_param_write) {
        return amxd_status_function_not_implemented;
    }

    client_info_t* info = (client_info_t*) object->priv;
    when_null_trace(info, exit, ERROR, "No private dat link to the client");

    status = amxd_status_ok;
    renew_enabled = amxc_var_dyncast(bool, args);
    when_false(renew_enabled, exit);

    if(!info->flags.bit.enable || (!info->flags.bit.req_addr && !info->flags.bit.req_pref)) {
        // Do nothing if DHCPv6 client is disabled
        SAH_TRACEZ_INFO(ME, "func release disabled");
    } else {
        info->flags.bit.force_stop = 1;
        dm_client_status(object, info->flags.bit.enable ? dhcpv6c_status_error_misconfigure : dhcpv6c_status_disabled);
        dm_dhcpv6c_start_stop_client(info);
        info->flags.bit.force_stop = 0;
        dm_dhcpv6c_start_stop_client(info);
    }

exit:
    amxc_var_set(int32_t, retval, ret);
    return status;
}

static void read_statistics(amxd_object_t* client_object, amxc_var_t* ret) {
    amxc_var_t params;
    const char* module = NULL;
    client_info_t* info = NULL;
    int retval = -1;

    amxc_var_init(&params);

    when_null_trace(client_object, exit, ERROR, "Client object is null");
    when_null_trace(ret, exit, ERROR, "ret is null");

    info = (client_info_t*) client_object->priv;
    when_null_trace(info, exit, ERROR, "Cannot get private data from client object");

    module = GET_CHAR(amxd_object_get_param_value(client_object, "Controller"), NULL);
    when_str_empty_trace(module, exit, ERROR, "Cannot get client controller");

    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "ifname", info->intf_name);

    retval = amxm_execute_function(module, module, "get-statistics", &params, ret);
    when_failed_trace(retval, exit, ERROR, "Mod %s, get-statistics returned %d", module, retval);

exit:
    amxc_var_clean(&params);
    return;
}

amxd_status_t _client_read_stats(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* client = amxd_object_get_parent(object);
    amxc_var_t result;
    amxc_var_t* stats = NULL;

    amxc_var_init(&result);

    when_false_status(reason == action_object_read, exit, status = amxd_status_function_not_implemented);

    status = amxd_action_object_read(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    read_statistics(client, &result);
    stats = GETI_ARG(&result, 0);

    amxc_var_for_each(var, retval) {
        if(strcmp(amxc_var_key(var), "Solicit") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_solicit"));
        } else if(strcmp(amxc_var_key(var), "Advertise") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_advertise"));
        } else if(strcmp(amxc_var_key(var), "Request") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_request"));
        } else if(strcmp(amxc_var_key(var), "Confirm") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_confirm"));
        } else if(strcmp(amxc_var_key(var), "Renew") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_renew"));
        } else if(strcmp(amxc_var_key(var), "Rebind") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_rebind"));
        } else if(strcmp(amxc_var_key(var), "Reply") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_reply"));
        } else if(strcmp(amxc_var_key(var), "Release") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_release"));
        } else if(strcmp(amxc_var_key(var), "Decline") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_decline"));
        } else if(strcmp(amxc_var_key(var), "Reconfigure") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_reconfigure"));
        } else if(strcmp(amxc_var_key(var), "InformationRequest") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_information_request"));
        } else if(strcmp(amxc_var_key(var), "DiscardedPackets") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_discarded_packets"));
        } else if(strcmp(amxc_var_key(var), "TransmitFailure") == 0) {
            amxc_var_set(uint32_t, var, GET_UINT32(stats, "dhcp_transmit_failures"));
        }
    }

exit:
    amxc_var_clean(&result);
    return status;
}

amxd_status_t _return_false(UNUSED amxd_object_t* object,
                            UNUSED amxd_param_t* param,
                            amxd_action_t reason,
                            UNUSED const amxc_var_t* const args,
                            amxc_var_t* const retval,
                            UNUSED void* priv) {
    if(reason != action_param_read) {
        return amxd_status_function_not_implemented;
    }
    amxc_var_set(bool, retval, false);
    return amxd_status_ok;
}