/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_dhcpv6client.h"
#include "dm_client.h"
#include "dm_client_duid.h"
#include "client_info.h"
#include "firewall.h"

#define str_empty(x) (x == NULL || *x == 0)

void _dhcpv6c_interface_changed(UNUSED const char* const event_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv6c_get_dm(), event_data);
    client_info_t* info = NULL;

    when_null_trace(client, exit, ERROR, "No client object found for change of interface");
    when_null_trace(client->priv, exit, ERROR, "No private data for client object found");

    info = (client_info_t*) client->priv;

    client_info_enable(info);
    dm_client_init(client);
exit:
    return;
}

void _dhcpv6c_object_changed(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv6c_get_dm(), event_data);
    client_info_t* info = NULL;
    amxc_var_t* enable_has_changed = GETP_ARG(event_data, "parameters.Enable");

    when_null_trace(client, exit, ERROR, "No client object found when change detected");
    SAH_TRACEZ_INFO(ME, "<%s> changed", amxd_object_get_name(client, AMXD_OBJECT_NAMED));
    when_null_trace(client->priv, exit, ERROR, "No private data of the client object found");

    info = (client_info_t*) client->priv;

    info->flags.bit.enable = amxd_object_get_value(bool, client, "Enable", NULL);
    info->flags.bit.req_addr = amxd_object_get_value(bool, client, "RequestAddresses", NULL);
    info->flags.bit.req_pref = amxd_object_get_value(bool, client, "RequestPrefixes", NULL);

    if(enable_has_changed != NULL) {
        dm_client_init(client);
    } else {
        dm_client_status(client, info->flags.bit.enable ? dhcpv6c_status_error_misconfigure : dhcpv6c_status_disabled);
    }
    dm_dhcpv6c_start_stop_client(info);
exit:
    return;
}

void _dhcpv6c_instance_added(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(dhcpv6c_get_dm(), event_data);
    amxd_object_t* instance = amxd_object_get_instance(obj, NULL, GET_UINT32(event_data, "index"));
    client_info_t* info = NULL;

    when_null_trace(instance, exit, ERROR, "Failed to find the added instance");

    client_info_create(instance);
    when_null_trace(instance->priv, exit, ERROR, "Cannot access private data of client object after creation");

    info = (client_info_t*) instance->priv;

    dm_client_status(instance, info->flags.bit.enable ? dhcpv6c_status_error_misconfigure : dhcpv6c_status_disabled);
exit:
    return;
}

void _dhcpv6c_timer_changed(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_object_t* client = amxd_dm_signal_get_object(dhcpv6c_get_dm(), event_data);
    int32_t time = GET_UINT32(event_data, "parameters.ResetOnPhysDownTimeout.to");
    client_info_t* info = NULL;
    when_null_trace(client, exit, ERROR, "object is null");
    info = (client_info_t*) client->priv;

    SAH_TRACEZ_INFO(ME, "time <%d> changed", time);

    //timer is started need to restart it with good timing
    if((amxp_timer_get_state(info->timer_physdown) == amxp_timer_started) || (amxp_timer_get_state(info->timer_physdown) == amxp_timer_running)) {
        amxp_timer_stop(info->timer_physdown);
        if(time > 0) {
            SAH_TRACEZ_INFO(ME, "Timer started");
            amxp_timer_start(info->timer_physdown, time);
        }
        // directly shutdown if timer == 0
        if(time == 0) {
            SAH_TRACEZ_INFO(ME, "Directly stopped");
            info->flags.bit.intf_exist = 0;
            dm_dhcpv6c_start_stop_client(info);
        }
        // otherwise, never shutdown
    }
exit:
    return;
}

static int instance_release(UNUSED amxd_object_t* templ,
                            amxd_object_t* instance,
                            UNUSED void* priv) {
    client_info_t* info = NULL;
    int status = 1;

    when_null_trace(instance, exit, ERROR, "No DHCPv6 client object given to release");
    info = (client_info_t*) instance->priv;
    when_null_trace(info, exit, ERROR, "No internal data found for the client");
    status = 0;
    when_false(info->flags.bit.req_addr || info->flags.bit.req_pref, exit);
    info->flags.bit.force_stop = 1;
    dm_client_status(instance, info->flags.bit.enable ? dhcpv6c_status_error_misconfigure : dhcpv6c_status_disabled);
    dm_dhcpv6c_start_stop_client(info);
    info->flags.bit.force_stop = 0;
exit:
    return status;
}

void _dhcpv6c_ipv6_going_down(UNUSED const char* const event_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    if(!str_empty(GET_CHAR(event_data, "Interface"))) {
        amxc_string_setf(&path, "[Interface=='Device.%s' && Enable==true].", GET_CHAR(event_data, "Interface"));
    } else {
        amxc_string_setf(&path, "[Enable==true].");
    }

    amxd_object_for_all(amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client."), amxc_string_get(&path, 0), instance_release, NULL);
    amxc_string_clean(&path);
}

void _dhcpv6c_sol_max_rt_changed(UNUSED const char* const event_name,
                                 const amxc_var_t* const event_data,
                                 UNUSED void* const priv) {
    amxd_object_t* retransmission_obj = amxd_dm_signal_get_object(dhcpv6c_get_dm(), event_data);
    amxd_object_t* client = amxd_object_get_parent(retransmission_obj);
    client_info_t* info = NULL;
    bool enable = false;
    char* status = NULL;

    when_null_trace(client, exit, ERROR, "No client object found for change of interface");
    when_null_trace(client->priv, exit, ERROR, "No private data for client object found");

    enable = amxd_object_get_value(bool, client, "Enable", NULL);
    status = amxd_object_get_value(cstring_t, client, "Status", NULL);

    when_true_trace(str_empty(status), exit, ERROR, "The Status parameter of [%s] is empty",
                    amxd_object_get_name(client, AMXD_OBJECT_NAMED));

    info = (client_info_t*) client->priv;
    if(enable && (strcmp(status, "Enabled") == 0)) {
        SAH_TRACEZ_INFO(ME, "Restart the odhcp6c for the client: %s",
                        amxd_object_get_name(client, AMXD_OBJECT_NAMED));
        dm_dhcpv6c_start_stop_client(info);
    }

exit:
    free(status);
    return;
}