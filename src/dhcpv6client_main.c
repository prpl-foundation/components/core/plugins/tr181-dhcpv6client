/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "dm_dhcpv6client.h"
#include "dm_server.h"
#include "dm_options.h"
#include "firewall.h"

typedef struct _dhcpv6_client {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxm_shared_object_t* so;
} _dhcpv6_client_t;

static _dhcpv6_client_t app;

amxd_dm_t* dhcpv6c_get_dm(void) {
    return app.dm;
}

amxc_var_t* dhcpv6c_get_config(void) {
    return &app.parser->config;
}

amxo_parser_t* dhcpv6c_get_parser(void) {
    return app.parser;
}

static int dhcpv6c_load_controllers(void) {
    int retval = -1;
    amxd_object_t* dhcpv6_obj = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client");
    const char* module_dir = GET_CHAR(dhcpv6c_get_config(), "module-client-dir");
    const amxc_var_t* controllers =
        amxd_object_get_param_value(dhcpv6_obj, "ClientSupportedControllers");
    amxc_var_t lcontrollers;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", module_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                        name, amxc_string_get(&mod_path, 0));
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        when_failed_trace(retval, exit, WARNING,
                          "Loading controller '%s' failed", name);
    }

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return retval;
}


static int load_module(const char* dir, const char* module, const char* controller) {
    const char* path = NULL;
    amxc_string_t path_str;
    int rv = -1;

    amxc_string_init(&path_str, 0);
    when_str_empty_trace(module, exit, ERROR, "controller string is empty");

    amxc_string_setf(&path_str, "%s/%s.so", dir, module);
    path = amxc_string_get(&path_str, 0);

    SAH_TRACEZ_NOTICE(ME, "Load module[%s] for %s", path, controller);

    rv = amxm_so_open(&app.so, controller, path);
    when_failed_trace(rv, exit, WARNING,
                      "Loading controller '%s' failed", controller);
exit:
    amxc_string_clean(&path_str);
    return rv;
}

static int dhcpv6c_load_fw_controllers(void) {
    int retval = -1;
    amxd_object_t* dhcpv6_obj = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client");
    const char* module_dir = GET_CHAR(dhcpv6c_get_config(), "fw-directory");
    const amxc_var_t* controllers =
        amxd_object_get_param_value(dhcpv6_obj, "SupportedFWControllers");
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);
    when_str_empty_trace(module_dir, exit, ERROR, "Missing or empty " \
                         "parameter 'fw-directory'");
    retval = amxd_status_parameter_not_found;
    when_null_trace(controllers, exit, ERROR, "Missing parameter 'ClientSupportedControllers'");
    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        when_null_trace(name, exit, ERROR, "Can't get controller name");
        amxc_string_setf(&mod_path, "%s/%s.so", module_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' (file = %s)",
                        name, amxc_string_get(&mod_path, 0));
        retval = load_module(module_dir, name, "fw");
        when_failed_trace(retval, exit, WARNING,
                          "Loading controller '%s' failed", name);
    }
    retval = 0;
exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    return retval;
}

/**
   @brief
   Register function(s) for use in modules specified in parameter
   Controller

   @return
   0 success.
   3 failure
 */
static int dhcpv6c_register_func(void) {
    int retval = 3;
    amxm_shared_object_t* so = amxm_get_so("self");
    amxm_module_t* mod = NULL;
    when_null(so, exit);

    when_failed(amxm_module_register(&mod, so, MOD_CORE), exit);

    retval = amxm_module_add_function(mod, "client-stopped", dm_client_stopped);
    retval |= amxm_module_add_function(mod, "client-started", dm_client_started);
    retval |= amxm_module_add_function(mod, "client-misconfigured", dm_client_misconfigured);
    retval |= amxm_module_add_function(mod, "server-add", dm_server_add);
    retval |= amxm_module_add_function(mod, "server-remove", dm_server_remove);
    retval |= amxm_module_add_function(mod, "options-add", dm_options_add);
    retval |= amxm_module_add_function(mod, "update-timing", dm_config_timing_set);
    SAH_TRACEZ_INFO(ME, "Registering functions returned %d", retval);
    retval = (retval != 0) ? 3 : 0;
exit:
    return retval;
}

static void unsubscribe_all_clients_netmodel(void) {
    amxd_object_t* clients = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.");
    when_null_trace(clients, exit, ERROR, "No client(s) found!");

    amxd_object_for_each(instance, it, clients) {
        amxd_object_t* client = amxc_container_of(it, amxd_object_t, it);
        client_info_clear((client_info_t*) client->priv);
        client->priv = NULL;
    }
exit:
    return;
}

static int dhcpv6c_subscribe_signals(void) {
    int ret = 1;
    amxb_bus_ctx_t* ip_ctx = amxb_be_who_has("IP.");

    when_null_trace(ip_ctx, exit, ERROR, "No bus found for IP");

    ret = amxb_subscribe(ip_ctx, "IP.", "notification == 'ip:ipv6_going_down'", _dhcpv6c_ipv6_going_down, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe to IP events");
exit:
    return ret;
}

static int dhcpv6c_init (amxd_dm_t* dm,
                         amxo_parser_t* parser) {
    int retval = -1;
    app.dm = dm;
    app.parser = parser;

    when_failed_trace(dhcpv6c_register_func(), exit, ERROR, "failed to register " \
                      "exported functions (%d)", retval);


    retval = dhcpv6c_load_controllers();
    when_failed_trace(retval, exit, ERROR, "failed to load 1 or " \
                      "more controllers (%d)", retval);
    retval = dhcpv6c_load_fw_controllers();
    when_failed_trace(retval, exit, ERROR, "failed to load 1 or " \
                      "more controllers (%d)", retval);

    when_false_trace(netmodel_initialize(), exit, ERROR,
                     "Failed to init netmodel");

    retval = dhcpv6c_subscribe_signals();
    when_failed_trace(retval, exit, ERROR, "Failed to subscribe to signals");

    retval = 0;
exit:
    return retval;
}

static int dhcpv6c_clean (void) {
    unsubscribe_all_clients_netmodel();
    firewall_cleanup();
    netmodel_cleanup();
    app.dm = NULL;
    app.parser = NULL;
    amxm_close_all();

    return 0;
}

int _dhcpv6client_main(int reason,
                       amxd_dm_t* dm,
                       amxo_parser_t* parser) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "DHCPv6 client plugin, reason: %d", reason);
    switch(reason) {
    case 0:
        // START
        retval = dhcpv6c_init(dm, parser);
        break;
    case 1:
        // STOP
        retval = dhcpv6c_clean();
        break;
    }
    SAH_TRACEZ_INFO(ME, "DHCPv6 client plugin, retval: %d", retval);

    return retval;
}
