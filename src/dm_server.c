/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "dm_client.h"
#include "dm_server.h"
#include "dm_options.h"

amxd_object_t* dm_get_server(amxd_object_t* dm_client,
                             const char* duid) {
    amxd_object_t* dm_server = NULL;
    when_null(dm_client, exit);
    when_str_empty(duid, exit);
    dm_server = amxd_object_findf(dm_client, "Server.[DUID=='%s']", duid);
exit:
    return dm_server;
}

/*
   Contents of parameter:
   args = {
      Interface = "IP.Interface.2.",
      SourceAddress = "",
      DUID = "",
      InformationRefreshTime = ""
   }
 */
int dm_server_add(UNUSED const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret) {
    int retval = -1;
    const char* src_addr = GET_CHAR(args, "SourceAddress");
    const char* server_duid = GET_CHAR(args, "DUID");
    const char* time = GET_CHAR(args, "InformationRefreshTime");
    char* path = NULL;
    amxd_object_t* client = dm_get_client(args);
    amxd_object_t* server = NULL;
    amxd_trans_t trans;

    SAH_TRACEZ_INFO(ME, "Server \"%s\", DUID %s", src_addr, server_duid);
    when_str_empty(src_addr, exit);
    when_str_empty(server_duid, exit);
    when_null(client, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    server = dm_get_server(client, server_duid);
    if(server == NULL) {
        amxd_trans_select_object(&trans, amxd_object_get(client, "Server"));
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(cstring_t, &trans, "DUID", server_duid);
    } else {
        amxd_trans_select_object(&trans, server);
    }
    amxd_trans_set_value(cstring_t, &trans, "SourceAddress", src_addr);
    // Optional parameter
    if((time != NULL) && (*time != '\0')) {
        amxd_trans_set_value(cstring_t, &trans, "InformationRefreshTime", time);
    }
    retval = amxd_trans_apply(&trans, dhcpv6c_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(retval, exit, ERROR, "failed to create/update " \
                      "server (%d)", retval);

    server = dm_get_server(client, server_duid);
    path = amxd_object_get_path(server, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    retval = 0;
exit:
    if(retval == 0) {
        amxc_var_set(cstring_t, ret, path);
    } else {
        amxc_var_set(cstring_t, ret, "");
    }
    free(path);
    return 0;
}

/*
   Contents of parameter:
   args = {
      Interface = "IP.Interface.2.",
      DUID = ""
   }
 */
int dm_server_remove(UNUSED const char* function_name,
                     amxc_var_t* args,
                     amxc_var_t* ret) {
    int retval = -1;
    char* path = NULL;
    const char* server_duid = GET_CHAR(args, "DUID");
    amxd_object_t* client = dm_get_client(args);
    amxd_object_t* server = NULL;

    SAH_TRACEZ_INFO(ME, "Remove server with duid '%s'", server_duid);
    when_null_trace(client, exit, ERROR, "DM Client null");
    server = amxd_object_findf(client, ".Server.[DUID=='%s']", server_duid);
    when_null(server, exit);
    path = amxd_object_get_path(server, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);

    amxd_object_emit_del_inst(server);
    amxd_object_delete(&server);
    SAH_TRACEZ_INFO(ME, "path '%s'", path);
    dm_options_remove(path, client);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    free(path);
    return 0;
}
