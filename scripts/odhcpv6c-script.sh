#!/bin/sh
json=''
IFS=$'\n'
for s in `env`; do
  # get key name
  key=${s%%=*}
  # get rest of line, remove first character ('=') and " \ characters
  value=${s#$key}
  value=${value:1}
  value=${value//\"/}
  value=${value//'\'/}
  json=$json', "'$key'": "'$value'"'
done

data='"phys_interf": "'$1'", "action": "'$2'"'$json
fncdata='{ "mod_name": "mod-odhcp6c", "fnc": "odhcp6c-script", "data": { '$data' } }'

ubus call DHCPv6Client update "$fncdata" 