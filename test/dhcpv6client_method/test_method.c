/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../common/common_functions.h"

#include "dhcpv6client.h"
#include "dm_dhcpv6client.h"
#include "test_method.h"

#define UNUSED __attribute__((unused))

#define DHCPV6_TEST "../common/dhcpv6client_test.odl"
#define DUMMY_TEST_ODL "../common/dummy.odl"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

static char* exp_func_name = NULL;
static bool exp_restart = false;
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    bool firewall = strcmp(shared_object_name, "fw") == 0;
    const char* exp_mod_name = firewall ? "fw" : "mod-odhcp6c";
    const char* exp_obj_name = firewall ? "fw" : "mod-odhcp6c";
    char* str_args = amxc_var_dyncast(cstring_t, args);

    assert_string_equal(exp_obj_name, shared_object_name);
    assert_string_equal(exp_mod_name, module_name);
    when_null(exp_func_name, exit);
    assert_string_equal(exp_func_name, func_name);
    if(strcmp(func_name, "renew-dhcpv6c") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "start-dhcpv6c") == 0) {
        const char* exp_args = "SentOption:Tag:61,Value:00020001,ifname:eth0," \
            "Interface:Device.IP.Interface.2.,RequestedOptions:23,31";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "stop-dhcpv6c") == 0) {
        const char* exp_args = "ifname:eth0,Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "odhcp6c-script") == 0) {
        const char* exp_args = "action:started";
        if(strcmp(str_args, exp_args) == 0) {
            amxc_var_set(int32_t, ret, 0);
        } else {
            amxc_var_set(int32_t, ret, -1);
        }
    } else if(strcmp(func_name, "set_service")) {
        if(firewall) {
            amxc_var_set(int32_t, ret, 0);
        } else {
            amxc_var_set(int32_t, ret, -1);
        }
    } else if(strcmp(func_name, "delete_service")) {
        if(firewall) {
            amxc_var_set(int32_t, ret, 0);
        } else {
            amxc_var_set(int32_t, ret, -1);
        }
    } else {
        printf("mocking amxm_execute_function: %s %s\n",
               module_name, func_name);
        printf("args: '%s'\n", str_args);
        fflush(stdout);
        amxc_var_dump(args, STDOUT_FILENO);
        // Assert here
        assert_string_equal(func_name, " unknown-function ");
    }
    if(exp_restart) {
        exp_restart = false;
        exp_func_name = "start-dhcpv6c";
    }
exit:
    free(str_args);
    return 0;
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    return 0;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    amxo_resolver_ftab_add(&parser, "setService", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(&parser, "deleteService", AMXO_FUNC(dummy_function_get));

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    assert_int_equal(amxo_parser_parse_file(&parser, DHCPV6_TEST, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, DUMMY_TEST_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    _dummy_main(0, &dm, &parser);


    exp_func_name = NULL;
    assert_int_equal(_dhcpv6client_main(AMXO_START, &dm, &parser), 0);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    exp_func_name = NULL;
    assert_int_equal(_dhcpv6client_main(AMXO_STOP, &dm, &parser), 0);
    _dummy_main(1, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_method_client_started(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* client = NULL;
    char* str_status = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");
    exp_func_name = "start-dhcpv6c";
    assert_int_equal(dm_client_started("dm_client_started", &args, &ret), 0);
    assert_int_equal(GET_INT32(&ret, NULL), 0);

    client = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    assert_non_null(client);
    str_status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_string_equal(str_status, "Error_Misconfigured");
    free(str_status);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.0.");

    exp_func_name = "start-dhcpv6c";
    assert_int_equal(dm_client_started("dm_client_started", &args, &ret), -1);
    assert_int_equal(GET_INT32(&ret, NULL), -1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_method_client_misconfigured(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* client = NULL;
    char* str_status = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");

    assert_int_equal(dm_client_misconfigured("dm_client_misconfigured", &args, &ret), 0);
    assert_int_equal(GET_INT32(&ret, NULL), 0);

    client = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    assert_non_null(client);
    str_status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_string_equal(str_status, "Error_Misconfigured");
    free(str_status);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_method_client_renew(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);


    exp_func_name = "renew-dhcpv6c";

    assert_int_equal(_Renew(template, NULL, &args, &retval), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_client_release(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    exp_func_name = "stop-dhcpv6c";
    exp_restart = true;

    assert_int_equal(_Release(template, NULL, &args, &retval), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_client_update(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(&dm, "DHCPv6Client.");
    amxc_var_t* data = NULL;
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    exp_func_name = "";
    // test missing mod_name
    assert_int_equal(amxd_object_invoke_function(template, "update", &args, &retval), 0);
    assert_int_equal(GET_INT32(&retval, NULL), -2);
    amxc_var_clean(&retval);

    // test "mod_name" ok, missing "fnc"
    amxc_var_add_key(cstring_t, &args, "mod_name", "mod-odhcp6c");
    assert_int_equal(amxd_object_invoke_function(template, "update", &args, &retval), 0);
    assert_int_equal(GET_INT32(&retval, NULL), -2);
    amxc_var_clean(&retval);

    // test "mod_name" ok, "fnc" ok, missing "data"
    amxc_var_add_key(cstring_t, &args, "mod_name", "mod-odhcp6c");
    amxc_var_add_key(cstring_t, &args, "fnc", "odhcp6c-script");
    assert_int_equal(amxd_object_invoke_function(template, "update", &args, &retval), 0);
    assert_int_equal(GET_INT32(&retval, NULL), -2);
    amxc_var_clean(&retval);

    // test "mod_name" ok, "fnc" ok, "data" exists but contains no valid data
    amxc_var_add_key(cstring_t, &args, "mod_name", "mod-odhcp6c");
    amxc_var_add_key(cstring_t, &args, "fnc", "odhcp6c-script");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "action", "no valid action");
    exp_func_name = "odhcp6c-script";
    assert_int_equal(amxd_object_invoke_function(template, "update", &args, &retval), 0);
    assert_int_equal(GET_INT32(&retval, NULL), -1);
    amxc_var_clean(&retval);

    // test "mod_name" ok, "fnc" ok, "data" exists but contains "valid data"
    amxc_var_add_key(cstring_t, &args, "mod_name", "mod-odhcp6c");
    amxc_var_add_key(cstring_t, &args, "fnc", "odhcp6c-script");
    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "action", "started");
    exp_func_name = "odhcp6c-script";
    assert_int_equal(amxd_object_invoke_function(template, "update", &args, &retval), 0);
    assert_int_equal(GET_INT32(&retval, NULL), 0);
    amxc_var_clean(&retval);

    amxc_var_clean(&args);
}

void test_method_client_stopped(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* client = NULL;
    char* str_status = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");

    assert_int_equal(dm_client_stopped("dm_client_stopped", &args, &ret), 0);
    assert_int_equal(GET_INT32(&ret, NULL), 0);

    client = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    assert_non_null(client);
    str_status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_string_equal(str_status, "Error_Misconfigured");
    free(str_status);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
