/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <amxm/amxm.h>

#include "../common/common_functions.h"
#include "../common/mock_netmodel.h"

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "dm_options.h"
#include "dhcpv6client.h"
#include "test_config.h"

#define UNUSED __attribute__((unused))

#define DHCPV6_TEST "../common/dhcpv6client_test.odl"
#define DUMMY_TEST_ODL "../common/dummy.odl"
#define CONFIG_PATH "DHCPv6Client.Client.1.Config."

#define UNKNOWN_TIME "0001-01-01T00:00:00Z"

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return 0;
}

int test_setup(void** state) {
    amxo_parser_t* parser = NULL;

    amxut_bus_setup(state);

    parser = amxut_bus_parser();

    resolver_add_all_functions(parser);

    amxut_dm_load_odl(DHCPV6_TEST);
    amxut_dm_load_odl(DUMMY_TEST_ODL);

    assert_int_equal(_dhcpv6client_main(AMXO_START, amxut_bus_dm(), parser), 0);
    amxut_bus_handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_dhcpv6client_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    amxut_bus_handle_events();
    amxut_bus_teardown(state);

    return 0;
}

void test_renew(UNUSED void** state) {
    amxd_object_t* config = NULL;
    amxc_ts_t* time = NULL;
    amxc_ts_t null_time;
    amxc_ts_t ref_time;
    amxc_ts_t ref_time_2;
    amxc_var_t data;

    amxc_ts_parse(&null_time, UNKNOWN_TIME, strlen(UNKNOWN_TIME));
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(uint32_t, &data, "Action", DHCPV6_ACTION_TIMING);

    config = amxd_dm_findf(amxut_bus_dm(), "DHCPv6Client.Client.1.Config.");
    assert_non_null(config);

    amxut_dm_param_equals(bool, CONFIG_PATH, "T1Renewed", false);
    amxut_dm_param_equals(bool, CONFIG_PATH, "T2Renewed", false);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT1SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT2SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LeaseRenewedWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);

    amxut_timer_go_to_future_ms(10 * 1000);
    amxc_ts_now(&ref_time);

    dm_config_timing_set(NULL, &data, NULL);
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, CONFIG_PATH, "T1Renewed", false);
    amxut_dm_param_equals(bool, CONFIG_PATH, "T2Renewed", false);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT1SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT2SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LeaseRenewedWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);

    amxut_timer_go_to_future_ms(10 * 1000);
    amxc_ts_now(&ref_time_2);

    amxc_var_set(uint32_t, amxc_var_get_key(&data, "Action", AMXC_VAR_FLAG_DEFAULT), DHCPV6_ACTION_UPDATED);

    dm_config_timing_set(NULL, &data, NULL);
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, CONFIG_PATH, "T1Renewed", true);
    amxut_dm_param_equals(bool, CONFIG_PATH, "T2Renewed", false);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT1SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT2SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LeaseRenewedWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, ref_time_2.sec);
    free(time);

    amxc_var_clean(&data);
    return;
}

void test_rebound(UNUSED void** state) {
    amxd_object_t* config = NULL;
    amxc_ts_t* time = NULL;
    amxc_ts_t null_time;
    amxc_ts_t ref_time;
    amxc_var_t data;

    amxc_ts_parse(&null_time, UNKNOWN_TIME, strlen(UNKNOWN_TIME));
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(uint32_t, &data, "Action", DHCPV6_ACTION_REBOUND);

    config = amxd_dm_findf(amxut_bus_dm(), "DHCPv6Client.Client.1.Config.");
    assert_non_null(config);

    amxut_timer_go_to_future_ms(10 * 1000);
    amxc_ts_now(&ref_time);

    dm_config_timing_set(NULL, &data, NULL);
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, CONFIG_PATH, "T2Renewed", true);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT1SentWhen", NULL);
    assert_non_null(time);
    assert_int_not_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT2SentWhen", NULL);
    assert_non_null(time);
    assert_int_not_equal(time->sec, ref_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LeaseRenewedWhen", NULL);
    assert_non_null(time);
    assert_int_not_equal(time->sec, ref_time.sec);
    free(time);

    amxc_var_clean(&data);
    return;
}

void test_stop(UNUSED void** state) {
    amxd_object_t* config = NULL;
    amxc_ts_t* time = NULL;
    amxc_ts_t null_time;
    amxc_var_t data;

    amxc_ts_parse(&null_time, UNKNOWN_TIME, strlen(UNKNOWN_TIME));
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(uint32_t, &data, "Action", DHCPV6_ACTION_STOP);

    config = amxd_dm_findf(amxut_bus_dm(), "DHCPv6Client.Client.1.Config.");
    assert_non_null(config);

    dm_config_timing_set(NULL, &data, NULL);
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, CONFIG_PATH, "T1Renewed", false);
    amxut_dm_param_equals(bool, CONFIG_PATH, "T2Renewed", false);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT1SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LastT2SentWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);
    time = amxd_object_get_value(amxc_ts_t, config, "LeaseRenewedWhen", NULL);
    assert_non_null(time);
    assert_int_equal(time->sec, null_time.sec);
    free(time);

    amxc_var_clean(&data);
    return;
}
