/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "dhcpv6client.h"
#include "dm_dhcpv6client.h"

#include "../common/common_functions.h"

// 'empty' replacements for functions in mod-dmext.so
static int _matches_regexp(void) {
    return AMXB_STATUS_OK;
}
static int _is_valid_ipv6(void) {
    return AMXB_STATUS_OK;
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    // events
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv6c_interface_changed",
                                            AMXO_FUNC(_dhcpv6c_interface_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv6c_instance_added",
                                            AMXO_FUNC(_dhcpv6c_instance_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv6c_timer_changed",
                                            AMXO_FUNC(_dhcpv6c_timer_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "object_delete",
                                            AMXO_FUNC(_object_delete)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "client_object_delete",
                                            AMXO_FUNC(_client_object_delete)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv6c_object_changed",
                                            AMXO_FUNC(_dhcpv6c_object_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcpv6c_sol_max_rt_changed",
                                            AMXO_FUNC(_dhcpv6c_sol_max_rt_changed)), 0);

    // actions
    assert_int_equal(amxo_resolver_ftab_add(parser, "update",
                                            AMXO_FUNC(_update)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "client_renew",
                                            AMXO_FUNC(_client_renew)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "client_release",
                                            AMXO_FUNC(_client_release)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "matches_regexp",
                                            AMXO_FUNC(_matches_regexp)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_valid_ipv6",
                                            AMXO_FUNC(_is_valid_ipv6)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "return_false",
                                            AMXO_FUNC(_return_false)), 0);
    // methods
    assert_int_equal(amxo_resolver_ftab_add(parser, "_Release",
                                            AMXO_FUNC(_Release)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "_Renew",
                                            AMXO_FUNC(_Renew)), 0);
}
