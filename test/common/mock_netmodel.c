/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_netmodel.h"
#include "mock_amxp.h"
#include "client_info.h"

#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static amxb_bus_ctx_t ip_ctx;
static amxp_slot_fn_t managed_flag_callback;
static amxp_slot_fn_t other_flag_callback;
static amxp_slot_fn_t ip_router_up_callback;

typedef struct _ra_config {
    bool ip_router_up;
    bool managed;
    bool other;
} ra_config_t;

static ra_config_t ra_config = {1, 1, 1};

void ra_config_set_ip_router(bool flag, void* userdata) {
    amxc_var_t data;
    amxc_var_init(&data);

    ra_config.ip_router_up = flag;

    amxc_var_set(bool, &data, flag);
    ip_router_up_callback("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    amxc_var_clean(&data);
}

void ra_config_set_managed(bool flag, void* userdata) {
    amxc_var_t data;
    amxc_var_init(&data);

    ra_config.managed = flag;

    amxc_var_set(bool, &data, flag);
    managed_flag_callback("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    amxc_var_clean(&data);
    dhcpv6c_delay_stop_phys_down(NULL, userdata);
}

void ra_config_set_other(bool flag, void* userdata) {
    amxc_var_t data;
    amxc_var_init(&data);

    ra_config.other = flag;

    amxc_var_set(bool, &data, flag);
    other_flag_callback("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    amxc_var_clean(&data);
    dhcpv6c_delay_stop_phys_down(NULL, userdata);
}

void ra_config_set(bool router_up_flag, bool managed_flag, bool other_flag, void* userdata) {
    ra_config_set_ip_router(router_up_flag, userdata);
    ra_config_set_managed(managed_flag, userdata);
    ra_config_set_other(other_flag, userdata);
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const cstring_t path) {
    amxb_bus_ctx_t* ret_ctx = NULL;

    if(strncmp(path, "Device.IP.Interface.", 20) == 0) {
        ret_ctx = &ip_ctx;
    }

    return ret_ctx;
}

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_cleanup(void) {
    return;
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     UNUSED const char* flag,
                                                     const char* traverse,
                                                     amxp_slot_fn_t handler,
                                                     void* userdata) {
    amxc_var_t data;
    amxc_var_t* ht_data = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_true(strcmp(subscriber, "tr181-dhcpv6client") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);

    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));
    // 1. call function with no data
    handler("NetModel.Intf.ip-wan.Query.1.", &data, userdata);
    // 2. call function with empty strings
    ht_data = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, ht_data, "Address", "");
    amxc_var_add_key(cstring_t, ht_data, "NetDevName", "");
    handler("NetModel.Intf.ip-wan.Query.1.", &data, userdata);
    // 3. call with usefull data
    amxc_var_delete(&ht_data);
    ht_data = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, ht_data, "Address", "01:02:03:04:05:06");
    amxc_var_add_key(cstring_t, ht_data, "NetDevName", "eth0");
    handler("NetModel.Intf.ip-wan.Query.1.", &data, userdata);
    // 4. call with same data
    handler("NetModel.Intf.ip-wan.Query.1.", &data, userdata);
    // 5. call with different data
    amxc_var_delete(&ht_data);
    ht_data = amxc_var_add(amxc_htable_t, &data, NULL);
    amxc_var_add_key(cstring_t, ht_data, "Address", "06:05:04:03:02:01");
    amxc_var_add_key(cstring_t, ht_data, "NetDevName", "eth1");
    handler("NetModel.Intf.ip-wan.Query.1.", &data, userdata);
    amxc_var_clean(&data);
    return q;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* name,
                                                              UNUSED const char* flag,
                                                              const char* traverse,
                                                              amxp_slot_fn_t handler,
                                                              void* userdata) {
    amxc_var_t data;
    client_info_t* info = (client_info_t*) userdata;
    amxc_var_init(&data);

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_non_null(name);
    assert_true(strcmp(subscriber, "tr181-dhcpv6client") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);

    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));

    if(strcmp(name, "MACAddress") == 0) {
        // 1. call function with no data
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 2. call function with empty string
        amxc_var_set(cstring_t, &data, "");
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 3. call with usefull data
        amxc_var_set(cstring_t, &data, "01:02:03:04:05:06");
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 4. call with same data
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    } else if(strcmp(name, "NetDevName") == 0) {
        // 1. call function with no data
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 2. call with usefull data
        amxc_var_set(cstring_t, &data, "eth0");
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 3. call function with empty string, expect timer
        amxc_var_set(cstring_t, &data, "");
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        mock_amxp_trigger_timer(info->timer_physdown);
        // 4. call with usefull data
        amxc_var_set(cstring_t, &data, "eth0");
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
        // 5. call with same data
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    } else if(strcmp(name, "IPv6Router.Managed") == 0) {
        managed_flag_callback = handler;
        amxc_var_set(bool, &data, ra_config.managed);
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    } else if(strcmp(name, "IPv6Router.Other") == 0) {
        other_flag_callback = handler;
        amxc_var_set(bool, &data, ra_config.other);
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    } else {
        assert_string_equal(name, "unknown");
    }
    amxc_var_clean(&data);
    return q;
}

netmodel_query_t* __wrap_netmodel_openQuery_hasFlag(const char* intf,
                                                    const char* subscriber,
                                                    const char* name,
                                                    UNUSED const char* flag,
                                                    const char* traverse,
                                                    amxp_slot_fn_t handler,
                                                    void* userdata) {
    amxc_var_t data;
    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));

    amxc_var_init(&data);

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_non_null(name);
    assert_true(strcmp(subscriber, "tr181-dhcpv6client") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);

    if(strcmp(name, "iprouter-up") == 0) {
        ip_router_up_callback = handler;
        amxc_var_set(bool, &data, ra_config.ip_router_up);
        handler("NetModel.Intf.ip-wan.Query.2.", &data, userdata);
    }
    amxc_var_clean(&data);
    return q;
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    free(query);
}
