/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "../common/mock.h"
#include "../common/common_functions.h"

#include "dhcpv6client.h"
#include "dm_dhcpv6client.h"
#include "dm_client.h"
#include "dm_options.h"
#include "test_options.h"

#define UNUSED __attribute__((unused))

#define DHCPV6_TEST             "../common/dhcpv6client_test.odl"
#define DUMMY_TEST_ODL          "../common/dummy.odl"
#define DATA_ADD_OPTIONS        "../common/data/data_add_options.json"
#define DATA_ADD_OPTIONS2       "../common/data/data_add_options2.json"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

int __wrap_amxm_so_open(amxm_shared_object_t** so,
                        const char* shared_object_name,
                        const char* const path_to_so);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 UNUSED const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return 0;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    amxo_resolver_ftab_add(&parser, "setService", AMXO_FUNC(dummy_function_get));
    amxo_resolver_ftab_add(&parser, "deleteService", AMXO_FUNC(dummy_function_get));

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    resolver_add_all_functions(&parser);

    assert_int_equal(amxo_parser_parse_file(&parser, DHCPV6_TEST, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, DUMMY_TEST_ODL, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    _dummy_main(0, &dm, &parser);


    handle_events();
    assert_int_equal(_dhcpv6client_main(AMXO_START, &dm, &parser), 0);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_dhcpv6client_main(AMXO_STOP, &dm, &parser), 0);
    handle_events();

    _dummy_main(1, &dm, &parser);

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}


void test_options_add(UNUSED void** state) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;
    amxd_object_t* option_obj = NULL;
    char* server_path = NULL;
    amxd_object_t* client = NULL;
    char* str_status = NULL;

    amxc_var_init(&ret);
    data = dummy_read_json_from_file(DATA_ADD_OPTIONS);
    assert_non_null(data);
    assert_int_equal(dm_options_add("dm_options_add", data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_delete(&data);
    amxc_var_clean(&ret);

    data = dummy_read_json_from_file(DATA_ADD_OPTIONS2);
    assert_non_null(data);
    assert_int_equal(dm_options_add("dm_options_add", data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_delete(&data);
    amxc_var_clean(&ret);

    option_obj = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.ReceivedOption.1");
    server_path = amxd_object_get_value(cstring_t, option_obj, "Server", NULL);
    assert_string_equal(server_path, "DHCPv6Client.Client.1.Server.1.");
    free(server_path);

    option_obj = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.ReceivedOption.5");
    server_path = amxd_object_get_value(cstring_t, option_obj, "Server", NULL);
    assert_string_equal(server_path, "DHCPv6Client.Client.1.Server.2.");
    free(server_path);

    client = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.");
    assert_non_null(client);
    str_status = amxd_object_get_value(cstring_t, client, "Status", NULL);
    assert_string_equal(str_status, "Enabled");
    free(str_status);
}

void test_options_removed(UNUSED void** state) {
    amxd_object_t* client = NULL;
    amxd_object_t* option_obj = NULL;

    client = amxd_dm_findf(dhcpv6c_get_dm(), "DHCPv6Client.Client.1");
    assert_non_null(client);

    dm_options_remove("DHCPv6Client.Client.1.Server.2.", client);
    // should be removed
    option_obj = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.ReceivedOption.5");
    assert_null(option_obj);
    // should NOT be removed
    option_obj = amxd_dm_findf(&dm, "DHCPv6Client.Client.1.ReceivedOption.1");
    assert_non_null(option_obj);
}

void test_options_change_interface_event(UNUSED void** state) {
    amxc_var_t event_data;
    amxc_var_t* parameters = NULL;
    amxc_var_t* intf = NULL;

    amxc_var_init(&event_data);
    amxc_var_set_type(&event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &event_data, "object", "DHCPv6Client.Client.cpe-Client-1.");
    amxc_var_add_key(cstring_t, &event_data, "path", "DHCPv6Client.Client.1.");
    parameters = amxc_var_add_key(amxc_htable_t, &event_data, "parameters", NULL);
    intf = amxc_var_add_key(amxc_htable_t, parameters, "Interface", NULL);
    amxc_var_add_key(cstring_t, intf, "from", "Device.IP.Interface.2.");
    amxc_var_add_key(cstring_t, intf, "to", "Device.IP.Interface.4.");

    //exp_func_name = "start-dhcpv6c";
    _dhcpv6c_object_changed(NULL, &event_data, NULL);

    amxc_var_clean(&event_data);
}
