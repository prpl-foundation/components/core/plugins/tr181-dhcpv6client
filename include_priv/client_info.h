/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CLIENT_INFO_H__)
#define __CLIENT_INFO_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "netmodel/client.h"

struct info_ra_flags {
    uint8_t m_flag : 1;
    uint8_t o_flag : 1;
    uint8_t up_flag : 1;
    uint8_t rsvd : 6;

};

struct info_flags {
    uint8_t enable : 1;
    uint8_t intf_exist : 1;
    uint8_t mac_ok : 1;
    uint8_t firewall_up : 1;
    uint8_t req_addr : 1;
    uint8_t req_pref : 1;
    uint8_t client_started : 1;
    uint8_t force_stop : 1;
};

struct timer_reason {
    uint8_t r_intf : 1;
    uint8_t r_ra_up : 1;
    uint8_t rsvd : 6;
};

typedef struct _ra_info {
    netmodel_query_t* q_ra_up;
    netmodel_query_t* q_ra_managed_flag;
    netmodel_query_t* q_ra_other_flag;
    union {
        uint8_t all_flags;
        struct info_ra_flags bit;
    } flags;
} ra_info_t;

typedef struct _client_info {
    netmodel_query_t* q_isup;
    netmodel_query_t* q_mac;
    ra_info_t ra_info;
    amxd_object_t* obj;
    amxp_timer_t* timer_physdown;
    char* mac_addr;
    char* intf_name;
    char* intf_path;
    int32_t time;
    union {
        uint8_t all_flags;
        struct info_flags bit;
    } flags;
    union {
        uint8_t all_flags;
        struct timer_reason bit;
    } timer_flags;
} client_info_t;

void client_info_clear(client_info_t* info);
void client_info_create(amxd_object_t* obj);
void client_info_enable(client_info_t* info);

void open_ra_received_query(client_info_t* info);
void close_ra_received_query(client_info_t* info);

void dhcpv6c_delay_stop_phys_down(amxp_timer_t* local_timer,
                                  void* priv);

#ifdef __cplusplus
}
#endif

#endif // __CLIENT_INFO_H__
