%populate {
{% if (BDfn.hasAnyUpstream()) : %}
    object DHCPv6Client {
        object 'Client' {
            instance add('wan') {
                parameter 'Enable' = true;
                parameter 'Interface' = "${ip_intf_wan}";
                parameter 'RequestAddresses' = false;
                parameter 'RequestPrefixes' = true;
            }
        }
    }
{% endif; %}
}
