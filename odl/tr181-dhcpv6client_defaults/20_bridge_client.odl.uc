%populate {
{% if (!BDfn.hasAnyUpstream()) : %}
    object DHCPv6Client {
        object 'Client' {
{% for (let Bridge in BD.Bridges) : %}
            instance add('{{lc(Bridge)}}') {
                parameter 'Enable' = true;
                parameter 'Interface' = "${ip_intf_{{lc(Bridge)}}}";
                parameter 'RequestAddresses' = false;
                parameter 'RequestPrefixes' = false;
            }
{% endfor; %}
        }
    }
{% endif; %}
}
