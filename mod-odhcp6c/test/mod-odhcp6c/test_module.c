/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <errno.h>
#include <fcntl.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "mod_odhcp6c.h"
#include "odhcp6c_actions.h"
#include "test_module.h"

#define DATA_START_CLIENT       "../common/data/data_start_dhcpv6c-1.json"
#define SCRIPT_DATA_STARTED     "../common/data/script_data_started.json"
#define SCRIPT_DATA_RA_UPDATED  "../common/data/script_data_ra_updated.json"
#define SCRIPT_DATA_BOUND       "../common/data/script_data_bound.json"
#define SCRIPT_DATA_UPDATED     "../common/data/script_data_updated.json"
#define SCRIPT_DATA_STOPPED     "../common/data/script_data_stopped.json"

int __wrap_amxp_subproc_astart(amxp_subproc_t* const subproc,
                               amxc_array_t* cmd);
int __wrap_amxp_subproc_get_exitstatus(amxp_subproc_t* subproc);
int __wrap_amxp_subproc_kill(const amxp_subproc_t* const subproc,
                             const int sig);
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);
int __wrap_kill(pid_t pid, int sig);

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

/*
   read sig alarm is used to wait for a timer to run out so the callback
   function for this timer will be executed before continuing with the
   next test.
 */
static void read_sig_alarm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
    }
}

int __wrap_amxp_subproc_astart(amxp_subproc_t* const subproc,
                               amxc_array_t* array) {
    int index = 0;
    char* arg = "";
    const char* cmd[] = {
        "odhcp6c",
        "-v",
        "-f",
        "-s",
        "/etc/amx/tr181-dhcpv6client/odhcpv6c-script.sh",
        "-N",
        "none",
        "-P0",
        "-t120",
        "-C0",
        "-R",
        "-r",
        "31",
        "-x",
        "23:2a02:1802:94:37f0:dafb:5eff:fe5c:e090",
        "-x",
        "500:00020001",
        "eth0",
        NULL
    };
    while(arg != NULL) {
        arg = (char*) amxc_array_get_data_at(array, index);
        if((arg == NULL) || (cmd[index] == NULL)) {
            break;
        }
        assert_string_equal(cmd[index], arg);
        index++;
    }
    subproc->pid = 1234;
    return 0;
}

int __wrap_amxp_subproc_get_exitstatus(UNUSED amxp_subproc_t* subproc) {
    static int retval = 0;
    retval = !retval;
    return retval;
}

int __wrap_amxp_subproc_kill(UNUSED const amxp_subproc_t* const subproc,
                             UNUSED const int sig) {
    return 0;
}

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    static int nr_fnc_called = 0;
    const char* exp_mod_name = "dm-dhcpv6c";
    char* str_args = amxc_var_dyncast(cstring_t, args);

    assert_string_equal(exp_mod_name, module_name);
    if(strcmp(func_name, "server-add") == 0) {
        // Check presence of parameters:
        // Interface, DUID, SourceAddress and InformationRefreshTime
        const char* interface = GET_CHAR(args, "Interface");
        const char* duid = GET_CHAR(args, "DUID");
        const char* server = GET_CHAR(args, "SourceAddress");
        const char* info_refresh = GET_CHAR(args, "InformationRefreshTime");

        assert_non_null(interface);
        assert_non_null(duid);
        assert_non_null(server);

        assert_string_equal(interface, "Device.IP.Interface.2.");
        assert_string_equal(duid, "00030001d8fb5e5ce090");
        assert_string_equal(server, "fe80::dafb:5eff:fe5c:e090");
        if(++nr_fnc_called == 1) {
            assert_string_equal(info_refresh, "0001-01-01T00:00:00Z");
        } else if(nr_fnc_called == 2) {
            amxc_ts_t tsp;
            amxc_ts_t tnow;
            uint64_t result = 0;
            assert_int_equal(amxc_ts_parse(&tsp, info_refresh, strlen(info_refresh)), 0);
            amxc_ts_now(&tnow);
            result = tnow.sec + 0x0E10 - tsp.sec;
            assert_in_range(result, 0, 1);
        } else {
            // Assert here
            // amxm_execute_function with function server-add is only called 2 times
            assert_int_equal(nr_fnc_called, 2);
        }
    } else if(strcmp(func_name, "client-started") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "server-remove") == 0) {
        const char* exp_args = "DUID:00030001d8fb5e5ce090,Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "options-add") == 0) {
        static int nr_add_called = 0;
        const char* exp_args = NULL;
        // The second time this function is called should come from the update function
        if(++nr_add_called == 1) {
            exp_args = "Interface:Device.IP.Interface.2.,ReceivedOption:" \
                "Tag:21,Value:74657374," \
                "Tag:24,Value:0262650A736F66746174686F6D6503636F6D000A7" \
                "36F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D00," \
                "Tag:23,Value:2A021802009437F0DAFB5EFFFE5CE090," \
                "Tag:3,Value:0000000100000460000007030005001" \
                "820010470D39B9100D11DDB22996869A4000008C000000E06," \
                "Tag:1,Value:00030001021018010C01," \
                "Tag:2,Value:00030001D8FB5E5CE090,Tag:7,Value:FF," \
                "Tag:25,Value:000000010000046500000708001A0019" \
                "000008CA00000E103C20010470D39B91E00000000000000000," \
                "Tag:31,Value:2A021802009437F0DAFB5EFFFE5CE090";
        } else {
            exp_args = "Interface:Device.IP.Interface.2.,ReceivedOption:" \
                "Tag:32,Value:0E10," \
                "Tag:23,Value:2A021802009437F0DAFB5EFFFE5CE090," \
                "Tag:3,Value:0000000100000465000007080005001820010470D39B9100D11DDB22996869A4000008CA00000E10," \
                "Tag:1,Value:00030001021018010C01," \
                "Tag:2,Value:00030001D8FB5E5CE090," \
                "Tag:7,Value:FF," \
                "Tag:25,Value:000000010000046500000708001A0019000008CA00000E103C20010470D39B91E00000000000000000," \
                "Tag:31,Value:2A021802009437F0DAFB5EFFFE5CE090";
        }
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "client-stopped") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "client-misconfigured") == 0) {
        const char* exp_args = "Interface:Device.IP.Interface.2.";
        assert_string_equal(exp_args, str_args);
    } else if(strcmp(func_name, "update-timing") == 0) {
        uint32_t action = GET_UINT32(args, "Action");
        assert_string_equal("Device.IP.Interface.2.", GET_CHAR(args, "Interface"));
        if(action == DHCPV6_ACTION_NB) {
            printf("action not ok\n");
            fflush(stdout);
            assert_false(true);
        }
    } else {
        printf("mocking amxm_execute_function: %s %s\n",
               module_name, func_name);
        printf("args: '%s'\n", str_args);
        fflush(stdout);
        amxc_var_dump(args, STDOUT_FILENO);
        // Assert here
        assert_string_equal(func_name, " unknown-function ");
    }
    free(str_args);
    return 0;
}

int __wrap_kill(pid_t pid, int sig) {
    static int nr_fnc_called = 0;
    const int sig_nrs[] = { 0, SIGKILL, SIGUSR1, SIGTERM, 0, SIGKILL, SIGKILL };
    nr_fnc_called++;
    // kill is only called 7 times
    assert_false(nr_fnc_called > 7);
    assert_int_equal(1234, pid);
    assert_int_equal(sig_nrs[nr_fnc_called - 1], sig);
    return 0;
}

int test_setup(UNUSED void** state) {
    return 0;
}

int test_teardown(UNUSED void** state) {
    return 0;
}

void test_stop_error_dhcpv6(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    assert_int_equal(odhcp6c_set_state_stop("odhcp6c_stop", &data, &ret), 0);
    assert_int_not_equal(amxc_var_constcast(int32_t, &ret), 0);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

    handle_events();
}

static void test_start_dhcpv6client(int retval) {
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* change = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    change = dummy_read_json_from_file(DATA_START_CLIENT);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_move(&data, change);
    amxc_var_delete(&change);

    assert_int_equal(odhcp6c_set_state_start("odhcp6c_start", &data, &ret), 0);

    assert_int_equal(amxc_var_constcast(int32_t, &ret), retval);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_start_dhcpv6(UNUSED void** state) {
    test_start_dhcpv6client(0);
}

void test_2nd_start_dhcpv6(UNUSED void** state) {
    // previous client is killed before starting new one
    test_start_dhcpv6client(0);
}

static void test_user_script_dhcpv6(const char* phys_interf,
                                    const char* action,
                                    const char* data_args,
                                    int retval) {
    amxc_var_t* data = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    if(data_args != NULL) {
        data = dummy_read_json_from_file(data_args);
    }
    if(data == NULL) {
        amxc_var_new(&data);
        amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    }

    if(phys_interf != NULL) {
        amxc_var_add_key(cstring_t, data, "phys_interf", phys_interf);
    }
    if(action != NULL) {
        amxc_var_add_key(cstring_t, data, "action", action);
    }
    assert_int_equal(odhcp6c_script("odhcp6c_script", data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), retval);

    amxc_var_delete(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_user_script_started_dhcpv6_1(UNUSED void** state) {
    test_user_script_dhcpv6(NULL, "started", SCRIPT_DATA_STARTED, -1);
}

void test_user_script_started_dhcpv6_2(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", "started", SCRIPT_DATA_STARTED, 0);
}

void test_user_script_bound_dhcpv6(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", "bound", SCRIPT_DATA_BOUND, 0);
}

void test_renew_dhcpv6(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(odhcp6c_renew("odhcp6c_renew", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_user_script_updated_dhcpv6(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", "updated", SCRIPT_DATA_UPDATED, 0);
}

void test_user_script_no_action_dhcpv6(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", NULL, SCRIPT_DATA_STARTED, -1);
}

void test_user_script_invalid_action_dhcpv6(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", "invalid-action", SCRIPT_DATA_STARTED, -1);
}

void test_stop_dhcpv6(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    alarm(2);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(odhcp6c_set_state_stop("odhcp6c_stop", &data, &ret), 0);
    assert_int_equal(amxc_var_constcast(int32_t, &ret), 0);
    read_sig_alarm();

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    handle_events();
}

void test_3rd_start_dhcpv6(UNUSED void** state) {
    // dummy call to __wrap_amxp_subproc_get_exitstatus to toggle return value
    __wrap_amxp_subproc_get_exitstatus(NULL);

    test_start_dhcpv6client(0);
}

void test_user_script_stopped_dhcpv6(UNUSED void** state) {
    test_user_script_dhcpv6("eth0", "stopped", SCRIPT_DATA_STOPPED, 0);
}
