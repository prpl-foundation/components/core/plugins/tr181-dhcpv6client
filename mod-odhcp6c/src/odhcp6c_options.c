/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <v4v6option.h>

#include "mod_odhcp6c.h"
#include "odhcp6c_options.h"
#include "odhcp6c_actions.h"

typedef struct {
    const char* odhcp6c_name;
    uint32_t nr;
    amxc_string_t* (*convert)(const char* interface, uint32_t opt, const char* value);
} odhcp6c_name_conv_t;

static amxc_string_t* option_cnv_str2hexstr(UNUSED const char* interface,
                                            UNUSED uint32_t opt,
                                            const char* value) {
    uint32_t length = strlen(value);
    amxc_string_t* str_value = NULL;

    amxc_string_new(&str_value, 0);
    when_false(length != 0, exit);
    amxc_string_bytes_2_hex_binary(str_value, value, length, NULL);
exit:
    return str_value;
}

static amxc_string_t* domains_str2hexstr(UNUSED const char* interface,
                                         uint32_t opt,
                                         const char* value) {
    amxc_string_t* hexstr = NULL;
    amxc_var_t src;
    amxc_var_t dest;
    char* buffer = NULL;

    amxc_var_init(&src);
    amxc_var_init(&dest);
    amxc_string_new(&hexstr, 0);

    amxc_var_set(cstring_t, &src, value);

    when_false_trace(opt <= USHRT_MAX, exit, ERROR, "Invalid option: %u", opt);

    dhcpoption_v6_option_to_hex(opt, &dest, &src);

    buffer = amxc_var_take(cstring_t, &dest);
    when_null(buffer, exit);

    amxc_string_push_buffer(hexstr, buffer, strlen(buffer) + 1);

exit:
    amxc_var_clean(&src);
    amxc_var_clean(&dest);
    return hexstr;
}

static amxc_var_t* option_split_string(const char* value) {
    int retval = -1;
    amxc_string_t str;
    amxc_var_t* varlist = NULL;

    amxc_var_new(&varlist);
    amxc_string_init(&str, 0);
    when_str_empty(value, exit);
    amxc_string_set(&str, value);
    retval = (int) amxc_string_ssv_to_var(&str, varlist, NULL);

    if(retval != AMXC_STRING_SPLIT_OK) {
        SAH_TRACEZ_WARNING(ME, "Splitting option string returned %d", retval);
    }
exit:
    amxc_string_clean(&str);
    return varlist;
}

static amxc_string_t* option_cnv_ipv6_2hexstr(UNUSED const char* interface,
                                              UNUSED uint32_t opt,
                                              const char* value) {
    struct in6_addr buf;
    amxc_string_t* result = NULL;
    amxc_var_t* list = option_split_string(value);

    amxc_string_new(&result, 0);
    amxc_var_for_each(var, list) {
        char* str_ipv6 = amxc_var_dyncast(cstring_t, var);
        when_str_empty_trace(str_ipv6, end_loop, WARNING, "Got NULL or empty IPv6 string");
        if(inet_pton(AF_INET6, str_ipv6, (void*) &buf) == 1) {
            amxc_string_t str_value;
            amxc_string_init(&str_value, 0);
            amxc_string_bytes_2_hex_binary(&str_value, (const char*) &buf,
                                           sizeof(struct in6_addr), NULL);
            amxc_string_appendf(result, "%s", amxc_string_get(&str_value, 0));
            amxc_string_clean(&str_value);
        }
end_loop:
        free(str_ipv6);
    }
    amxc_var_delete(&list);
    return result;
}

static amxc_string_t* option_3_str2hexstr(const char* interface,
                                          UNUSED uint32_t opt,
                                          const char* value) {
    amxc_string_t* result = NULL;
    uint32_t nr = 0;
    uint32_t bits = 0;
    uint32_t preferred = 0;
    uint32_t valid = 0;
    uint32_t t1 = 0;
    uint32_t t2 = 0;
    uint32_t min_preferred = UINT32_MAX;
    uint32_t min_valid = UINT32_MAX;
    struct in6_addr buf;
    amxc_var_t* list = option_split_string(value);

    amxc_string_new(&result, 0);
    amxc_var_for_each(var, list) {
        char* str = amxc_var_dyncast(cstring_t, var);
        char* pos = NULL;
        when_str_empty_trace(str, end_loop, WARNING, "Got NULL or empty string");
        pos = strchr(str, (int) '/');
        when_null(pos, end_loop);
        if(sscanf(pos, "/%u,%u,%u,%u,%u", &bits, &preferred, &valid, &t1, &t2) != 5) {
            goto end_loop;
        }
        *pos = '\0';
        if(inet_pton(AF_INET6, str, (void*) &buf) == 1) {
            amxc_string_t str_value;

            min_preferred = (preferred < min_preferred) ? preferred : min_preferred;
            min_valid = (valid < min_valid) ? valid : min_valid;
            amxc_string_init(&str_value, 0);
            amxc_string_bytes_2_hex_binary(&str_value, (const char*) &buf,
                                           sizeof(struct in6_addr), NULL);
            amxc_string_appendf(result, "00050018%s%08X%08X", amxc_string_get(&str_value, 0),
                                preferred, valid);
            amxc_string_clean(&str_value);
            nr++;
        }
end_loop:
        free(str);
    }
    if(nr > 0) {
        amxc_string_prependf(result, "00000001%08X%08X", t1,
                             t2);
        send_action(interface, DHCPV6_ACTION_TIMING);
    }
    amxc_var_delete(&list);
    return result;
}

static amxc_string_t* option_25_str2hexstr(const char* interface,
                                           UNUSED uint32_t opt,
                                           const char* value) {
    amxc_string_t* result = NULL;
    uint32_t nr = 0;
    uint32_t bits = 0;
    uint32_t preferred = 0;
    uint32_t valid = 0;
    uint32_t t1 = 0;
    uint32_t t2 = 0;
    uint32_t min_preferred = UINT32_MAX;
    uint32_t min_valid = UINT32_MAX;
    struct in6_addr buf;
    amxc_var_t* list = option_split_string(value);

    amxc_string_new(&result, 0);
    amxc_var_for_each(var, list) {
        char* str = amxc_var_dyncast(cstring_t, var);
        char* pos = NULL;
        when_str_empty_trace(str, end_loop, WARNING, "Got NULL or empty string");
        pos = strchr(str, (int) '/');
        when_null(pos, end_loop);
        if(sscanf(pos, "/%u,%u,%u,%u,%u", &bits, &preferred, &valid, &t1, &t2) != 5) {
            goto end_loop;
        }
        *pos = '\0';
        if(inet_pton(AF_INET6, str, (void*) &buf) == 1) {
            amxc_string_t str_value;
            min_preferred = (preferred < min_preferred) ? preferred : min_preferred;
            min_valid = (valid < min_valid) ? valid : min_valid;
            amxc_string_init(&str_value, 0);
            amxc_string_bytes_2_hex_binary(&str_value, (const char*) &buf,
                                           sizeof(struct in6_addr), NULL);
            amxc_string_appendf(result, "001A0019%08X%08X%02X%s", preferred, valid, bits,
                                amxc_string_get(&str_value, 0));
            amxc_string_clean(&str_value);
            nr++;
        }
end_loop:
        free(str);
    }
    if(nr > 0) {
        amxc_string_prependf(result, "00000001%08X%08X", t1,
                             t2);
        send_action(interface, DHCPV6_ACTION_TIMING);
    }
    amxc_var_delete(&list);
    return result;
}

static odhcp6c_name_conv_t names[] = {
    { "ADDRESSES", 3, option_3_str2hexstr },
    { "SIP_DOMAIN", 21, option_cnv_str2hexstr },
    { "SIP_IP", 22, option_cnv_ipv6_2hexstr },
    { "RDNSS", 23, option_cnv_ipv6_2hexstr },
    { "DOMAINS", 24, domains_str2hexstr },
    { "PREFIXES", 25, option_25_str2hexstr },
    { "SNTP_IP", 31, option_cnv_ipv6_2hexstr },
    { "NTP_IP", 56, option_cnv_ipv6_2hexstr },
    { "AFTR", 64, domains_str2hexstr },
    { NULL, 0, NULL }
};

static bool received_option_keyword(const char* interface,
                                    const char* key,
                                    const char* value,
                                    uint32_t* tag,
                                    amxc_string_t** ret) {
    odhcp6c_name_conv_t* opt = names;
    bool converted = false;
    while(opt->odhcp6c_name != NULL) {
        if(strcmp(key, opt->odhcp6c_name) == 0) {
            *tag = opt->nr;
            *ret = opt->convert(interface, opt->nr, value);
            converted = true;
            break;
        }
        opt++;
    }
    return converted;
}

void received_options_add(const char* server,
                          const char* interface,
                          amxc_var_t* args) {
    int retval = 0;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* var_opts = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    amxc_var_add_key(cstring_t, &data, "Server", server);
    var_opts = amxc_var_add_key(amxc_llist_t, &data, "ReceivedOption", NULL);
    amxc_var_for_each(var, args) {
        const char* key = amxc_var_key(var);
        const char* value = amxc_var_constcast(cstring_t, var);
        amxc_string_t* conv_value = NULL;
        amxc_var_t* option = NULL;
        uint32_t option_nr = 0;
        char dummy_char = 0;
        if(sscanf(key, "OPTION_%u%c", &option_nr, &dummy_char) == 1) {
            if(str_empty(value)) {
                SAH_TRACEZ_INFO(ME, "%s, option %u, value '%s' -> skip", key,
                                option_nr, value);
            } else {
                // odhcp6c component gives values as hexbinary strings, no conversion needed
                option = amxc_var_add(amxc_htable_t, var_opts, NULL);
                amxc_var_add_key(uint8_t, option, "Tag", option_nr);
                for(char* pntr = (char*) value; *pntr != '\0'; pntr++) {
                    *pntr = toupper(*pntr);
                }
                amxc_var_add_key(cstring_t, option, "Value", value);
                SAH_TRACEZ_INFO(ME, "%s, value '%s'", key, value);
            }
        } else if(received_option_keyword(interface, key, value, &option_nr, &conv_value)) {
            if(amxc_string_is_empty(conv_value) == true) {
                SAH_TRACEZ_INFO(ME, "%s, option %u, value '%s' -> skip", key,
                                option_nr, value);
            } else {
                option = amxc_var_add(amxc_htable_t, var_opts, NULL);
                amxc_var_add_key(uint8_t, option, "Tag", option_nr);
                amxc_var_add_key(cstring_t, option, "Value", amxc_string_get(conv_value, 0));
                SAH_TRACEZ_INFO(ME, "%s, option %u, value '%s' -> '%s'", key, option_nr,
                                value, amxc_string_get(conv_value, 0));
            }
            amxc_string_delete(&conv_value);
        }
    }
    retval = amxm_execute_function(NULL, MOD_CORE, "options-add", &data, &ret);
    (void) retval; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Fnc \"options-add\" returned %d", retval);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}
