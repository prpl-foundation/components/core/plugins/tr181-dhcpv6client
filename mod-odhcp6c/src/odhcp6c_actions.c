/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>

#include "mod_odhcp6c.h"
#include "odhcp6c_actions.h"
#include "odhcp6c_options.h"
#include "odhcp6c_list.h"

typedef struct {
    int (* func)(const char* interface, amxc_var_t* args);
    const char* action;
} odhcp6c_action_t;

static int actions_started (const char* interface, amxc_var_t* args);
static int actions_bound (const char* interface, amxc_var_t* args);
static int actions_updated (const char* interface, amxc_var_t* args);
static int actions_informed (const char* interface, amxc_var_t* args);
static int actions_ra_upd (const char* interface, amxc_var_t* args);
static int actions_rebound (const char* interface, amxc_var_t* args);
static int actions_stopped (const char* interface, amxc_var_t* args);
static int actions_unbound (const char* interface, amxc_var_t* args);

odhcp6c_action_t action_handlers[] = {
    { actions_started, "started" },   // The DHCPv6 client has been started
    { actions_bound, "bound" },       // A suitable server was found and addresses
                                      // or prefixes acquired
    { actions_informed, "informed" }, // A stateless information request returned
                                      // updated information
    { actions_updated, "updated" },   // Updated information was received
                                      // from the DHCPv6 server
    { actions_ra_upd, "ra-updated" }, // Updated information was received
                                      // from via Router Advertisement
    { actions_rebound, "rebound" },   // The DHCPv6 client switched to another server
    { actions_unbound, "unbound" },   // The DHCPv6 client lost all DHCPv6 servers
                                      // and will restart
    { actions_stopped, "stopped" },   // The DHCPv6 client has been stopped
    { NULL, NULL }
};

void send_action(const char* interface, enum action_t action) {
    amxc_var_t data;
    amxc_var_t ret;
    int retval = 1;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    amxc_var_add_key(uint32_t, &data, "Action", action);

    retval = amxm_execute_function(NULL, MOD_CORE, "update-timing", &data, &ret);
    when_failed_trace(retval, exit, ERROR, "Unable to update timing, update-timing call returned : %d", retval);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return;
}

static int actions_started(UNUSED const char* interface,
                           UNUSED amxc_var_t* args) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);

    retval = amxm_execute_function(NULL, MOD_CORE, "client-started", &data, &ret);
    (void) retval; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Fnc \"client-started\" returned %d", retval);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return 0;
}

static char* server_add(const char* interface,
                        const char* server,
                        const char* duid,
                        const char* info_refresh_time) {
    int retval = -1;
    char* path = NULL;
    char* tmp = NULL;
    char time[36];
    amxc_var_t data;
    amxc_var_t ret;
    amxc_ts_t tsp;

    when_str_empty(server, exit);
    when_str_empty(duid, exit);
    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    amxc_var_add_key(cstring_t, &data, "SourceAddress", server);
    amxc_var_add_key(cstring_t, &data, "DUID", duid);
    if(str_empty(info_refresh_time)) {
        SAH_TRACEZ_INFO(ME, "Option 32 (information refresh time) not received");
        strcpy(time, "0001-01-01T00:00:00Z");
    } else {
        amxc_ts_now(&tsp);
        tsp.sec += strtoul(info_refresh_time, &tmp, 16);
        amxc_ts_format_precision(&tsp, time, sizeof(time), 0);
    }
    amxc_var_add_key(cstring_t, &data, "InformationRefreshTime", time);

    retval = amxm_execute_function(NULL, MOD_CORE, "server-add", &data, &ret);
    path = amxc_var_dyncast(cstring_t, &ret);
    (void) retval; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Fnc \"server-add\" returned %d, server %s", retval, path);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
exit:
    return path;
}

static int actions_bound(const char* interface,
                         amxc_var_t* args) {
    char* server = server_add(interface, GET_CHAR(args, "SERVER"),
                              GET_CHAR(args, "OPTION_2"),
                              GET_CHAR(args, "OPTION_32"));
    received_options_add((const char*) server, interface, args);
    send_action(interface, DHCPV6_ACTION_BOUND);
    free(server);
    return 0;
}

static int actions_updated(const char* interface,
                           amxc_var_t* args) {
    char* server = server_add(interface, GET_CHAR(args, "SERVER"),
                              GET_CHAR(args, "OPTION_2"),
                              GET_CHAR(args, "OPTION_32"));
    received_options_add((const char*) server, interface, args);
    send_action(interface, DHCPV6_ACTION_UPDATED);
    free(server);
    return 0;
}

static int actions_informed(UNUSED const char* interface,
                            UNUSED amxc_var_t* args) {
    char* server = server_add(interface, GET_CHAR(args, "SERVER"),
                              GET_CHAR(args, "OPTION_2"),
                              GET_CHAR(args, "OPTION_32"));
    received_options_add((const char*) server, interface, args);
    free(server);
    return 0;
}

static int actions_ra_upd(UNUSED const char* interface,
                          UNUSED amxc_var_t* args) {
    return 0;
}

static int actions_rebound(const char* interface,
                           UNUSED amxc_var_t* args) {
    send_action(interface, DHCPV6_ACTION_REBOUND);
    return 0;
}

static int actions_unbound(const char* interface,
                           UNUSED amxc_var_t* args) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    amxc_var_add_key(cstring_t, &data, "DUID", GET_CHAR(args, "OPTION_2"));

    retval = amxm_execute_function(NULL, MOD_CORE, "server-remove", &data, &ret);
    (void) retval; // use variable when tracing is disabled
    SAH_TRACEZ_INFO(ME, "Fnc \"server-remove\" returned %d", retval);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return 0;
}

static int actions_stopped(const char* interface,
                           UNUSED amxc_var_t* args) {
    odhcp6c_client_stopped(interface);
    send_action(interface, DHCPV6_ACTION_STOP);
    return 0;
}

int actions_handler(const char* interface,
                    const char* action,
                    amxc_var_t* args) {
    int retval = -1;
    odhcp6c_action_t* handler = action_handlers;
    SAH_TRACEZ_INFO(ME, "Interface %s, action %s", interface, action);
    while((handler->action != NULL) && (strcmp(handler->action, action) != 0)) {
        handler++;
    }
    when_null(handler->action, exit);
    when_null(handler->func, exit);
    retval = handler->func(interface, args);
exit:
    return retval;
}
