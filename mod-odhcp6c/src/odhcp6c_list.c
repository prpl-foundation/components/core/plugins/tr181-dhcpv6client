/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "odhcp6c_list.h"

static amxc_llist_t* if_list = NULL;

void odhcp6c_list_init(void) {
    if(if_list == NULL) {
        amxc_llist_new(&if_list);
    }
}

static void odhcp6c_list_delete_item(amxc_llist_it_t* it) {
    odhcp6c_t* odhcp = amxc_container_of(it, odhcp6c_t, it);
    amxc_llist_it_take(it);

    amxc_var_delete(&(odhcp->latest_args));
    amxc_var_delete(&(odhcp->used_args));
    amxp_subproc_delete(&(odhcp->subproc));

    free(odhcp->interface);
    odhcp->interface = NULL;

    free(odhcp);
    odhcp = NULL;
}

odhcp6c_t* odhcp6c_list_find(const char* interf) {
    odhcp6c_t* ret_ptr = NULL;
    when_null(interf, exit);
    amxc_llist_for_each(it, if_list) {
        odhcp6c_t* odhcp = amxc_container_of(it, odhcp6c_t, it);
        if(strcmp(interf, odhcp->interface) == 0) {
            ret_ptr = odhcp;
            break;
        }
    }
exit:
    return ret_ptr;
}

odhcp6c_t* odhcp6c_list_find_intf(const char* ifname) {
    odhcp6c_t* ret_ptr = NULL;
    const char* used_ifname = NULL;

    when_null(ifname, exit);
    amxc_llist_for_each(it, if_list) {
        odhcp6c_t* odhcp = amxc_container_of(it, odhcp6c_t, it);
        used_ifname = GET_CHAR(odhcp->used_args, "ifname");
        if((used_ifname != NULL) && (strcmp(ifname, used_ifname) == 0)) {
            ret_ptr = odhcp;
            break;
        }
    }
exit:
    return ret_ptr;
}

int odhcp6c_list_delete(const char* interf) {
    int retval = -1;

    amxc_llist_for_each(it, if_list) {
        odhcp6c_t* odhcp = amxc_container_of(it, odhcp6c_t, it);
        if(strcmp(interf, odhcp->interface) == 0) {
            odhcp6c_list_delete_item(it);
            retval = 0;
            break;
        }
    }
    return retval;
}

void odhcp6c_list_all(odhcp6c_cb_fnc_t func) {
    when_null(func, exit);
    amxc_llist_for_each(it, if_list) {
        func(amxc_container_of(it, odhcp6c_t, it));
    }
exit:
    return;
}

odhcp6c_t* odhcp6c_list_add(amxc_var_t* args) {
    odhcp6c_t* odhcp = NULL;
    const char* interface = NULL;

    when_null_trace(args, exit, ERROR, "The given args parameter is invalid");
    interface = GET_CHAR(args, "Interface");
    when_null_trace(interface, exit, ERROR, "bad input interface value");
    when_false_trace(odhcp6c_list_find(interface) == NULL, exit, ERROR, "their is already a odhcp struct in the list");

    odhcp = (odhcp6c_t*) calloc(1, sizeof(odhcp6c_t));
    when_null_trace(odhcp, exit, ERROR, "Failed to execute calloc");

    odhcp->interface = strdup(interface);
    amxc_var_new(&odhcp->latest_args);
    amxc_var_new(&odhcp->used_args);
    amxc_llist_append(if_list, &(odhcp->it));
    amxp_subproc_new(&odhcp->subproc);
    odhcp->state = ODHCP_STOPPED;
exit:
    return odhcp;
}

void odhcp6c_list_delete_all(void) {
    amxc_llist_delete(&if_list, odhcp6c_list_delete_item);
}
