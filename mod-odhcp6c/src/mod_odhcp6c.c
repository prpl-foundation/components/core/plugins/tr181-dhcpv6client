/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "mod_odhcp6c.h"
#include "odhcp6c_list.h"
#include "odhcp6c_actions.h"
#include "v4v6option.h"

#define GRACEFUL_STOP_TIMEOUT 1000
#define NO_IA_PD INT32_MAX

/**********************************************************
* Function Prototypes
**********************************************************/

static void update_state(odhcp6c_t* odhcp6c);
static int odhcp6c_stop(odhcp6c_t* odhcp6c);
static void odhcp6c_stop_handler(const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv);
/**********************************************************
* Functions
**********************************************************/

static int odhcp6c_start_process(odhcp6c_t* odhcp6c,
                                 const char* req_opt,
                                 amxc_array_t* sent_opts) {
    int retval = -1;
    uint32_t index = 0;
    size_t nr_sent_opts = amxc_array_size(sent_opts); // returns 0 if ptr is NULL
    size_t nr_args = 11 + 2 * nr_sent_opts + ((req_opt == NULL) ? 0 : 2);
    amxc_array_t cmd;
    const char* ifname = NULL;
    char* sol_max_rt = NULL;
    char* dscp = NULL;
    amxc_string_t sol_max_arg;
    amxc_string_t dscp_arg;

    amxc_array_init(&cmd, nr_args);
    amxc_string_init(&sol_max_arg, 0);
    amxc_string_init(&dscp_arg, 0);

    when_null_trace(odhcp6c, exit, ERROR, "Bad odhcp input value");
    when_null_trace(odhcp6c->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false_trace(!amxp_subproc_is_running(odhcp6c->subproc), exit, ERROR, "trying to start the subproc, but the subproc is already running");
    when_null_trace(odhcp6c->used_args, exit, ERROR, "Could not find the odhcp used_args parameter");
    ifname = GET_CHAR(odhcp6c->used_args, "ifname");
    when_null_trace(ifname, exit, ERROR, "Could not find the odhcp ifname");

    sol_max_rt = amxc_var_dyncast(cstring_t, GET_ARG(odhcp6c->used_args, "sol_max_rt"));
    if(!str_empty(sol_max_rt)) {
        amxc_string_setf(&sol_max_arg, "-t%s", sol_max_rt);
    }

    dscp = amxc_var_dyncast(cstring_t, GET_ARG(odhcp6c->used_args, "dscp"));
    if(!str_empty(dscp)) {
        amxc_string_setf(&dscp_arg, "-C%s", dscp);
    }

    amxc_array_append_data(&cmd, (void*) "odhcp6c");
    amxc_array_append_data(&cmd, (void*) "-v");
    amxc_array_append_data(&cmd, (void*) "-f");
    amxc_array_append_data(&cmd, (void*) "-s");
    amxc_array_append_data(&cmd, (void*) "/etc/amx/tr181-dhcpv6client/odhcpv6c-script.sh");

    if(GET_BOOL(odhcp6c->used_args, "RequestAddresses") == false) {
        amxc_array_append_data(&cmd, (void*) "-N");
        amxc_array_append_data(&cmd, (void*) "none");
    }
    if(GET_BOOL(odhcp6c->used_args, "RequestPrefixes")) {
        amxc_array_append_data(&cmd, (void*) "-P0");
    }
    amxc_array_append_data(&cmd, (void*) amxc_string_get(&sol_max_arg, 0));
    amxc_array_append_data(&cmd, (void*) amxc_string_get(&dscp_arg, 0));
    amxc_array_append_data(&cmd, (void*) "-R");
    if(req_opt != NULL) {
        amxc_array_append_data(&cmd, (void*) "-r");
        amxc_array_append_data(&cmd, (void*) req_opt);
    }
    while(nr_sent_opts > 0) {
        amxc_array_append_data(&cmd, (void*) "-x");
        amxc_array_append_data(&cmd, amxc_array_get_data_at(sent_opts, index));
        index++;
        nr_sent_opts--;
    }

    /* Interface name should always be last */
    amxc_array_append_data(&cmd, (void*) ifname);
    retval = amxp_subproc_astart(odhcp6c->subproc, &cmd);
    when_false_trace(retval == 0, exit, ERROR, "Failed to start the subproc");
    SAH_TRACEZ_INFO(ME, "amxp_subproc_astart (odhcp6c %s, pid %d) ret %d", ifname, odhcp6c->subproc->pid, retval);
exit:
    free(sol_max_rt);
    free(dscp);
    amxc_string_clean(&sol_max_arg);
    amxc_string_clean(&dscp_arg);
    amxc_array_clean(&cmd, NULL);
    return retval;
}

static void odhcp6c_set_sent_options(amxc_var_t* opts,
                                     amxc_array_t* array) {
    size_t size = 0;
    when_null_trace(opts, exit, ERROR, "No SentOption defined");
    when_null_trace(array, exit, ERROR, "No array defined");

    amxc_var_for_each(var, opts) {
        size++;
    }
    amxc_array_grow(array, size);
    amxc_var_for_each(var, opts) {
        amxc_var_t* parsed_info = NULL;
        uint32_t length = 0;
        uint16_t tag = (uint16_t) GET_UINT32(var, "Tag");
        char* param_value = NULL;
        char* str = NULL;
        unsigned char* option = dhcpoption_option_convert2bin(GET_CHAR(var, "Value"), &length);
        amxc_var_new(&parsed_info);
        // Convert binary buffer to string(s) (amxc_var_t)
        dhcpoption_v6parse(parsed_info, tag, length, option);
        free(option);
        param_value = amxc_var_dyncast(cstring_t, parsed_info);
        amxc_var_delete(&parsed_info);

        length = 7 + strlen(param_value);
        str = (char*) calloc(1, length);
        sprintf(str, "%u:%s", tag, param_value);
        free(param_value);
        // memory of 'str'' is freed when cleaning up array
        amxc_array_append_data(array, (void*) str);
    }
exit:
    return;
}

static void odhcp6c_free_array_item(amxc_array_it_t* it) {
    free(it->data);
}

static int odhcp6c_start(odhcp6c_t* odhcp6c, amxc_var_t* args) {
    const char* reqoptions = NULL;
    int retval = -1;
    amxc_array_t array_opts;

    amxc_array_init(&array_opts, 0);

    when_null_trace(odhcp6c, exit, ERROR, "Invalid odhcp6c input parameter");
    when_null_trace(args, exit, ERROR, "Invalid args input parameter");

    reqoptions = GET_CHAR(args, "RequestedOptions");

    odhcp6c->state = ODHCP_STARTING;
    retval = amxp_slot_connect(amxp_subproc_get_sigmngr(odhcp6c->subproc), "stop", NULL, odhcp6c_stop_handler, (void*) odhcp6c);
    when_false_trace(retval == 0, exit, ERROR, "Failed to connect stop handler");

    odhcp6c_set_sent_options(GETP_ARG(args, "SentOption"), &array_opts);
    retval = odhcp6c_start_process(odhcp6c, (str_empty(reqoptions) == false ? reqoptions : NULL), &array_opts);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute odhcp6c_start_process");
exit:
    amxc_array_clean(&array_opts, odhcp6c_free_array_item);
    return retval;
}

void odhcp6c_client_stopped(const char* interface) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    when_str_empty_trace(interface, exit, ERROR, "Interface path missing");

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    retval = amxm_execute_function(NULL, MOD_CORE, "client-stopped", &data, &ret);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute amxm_execute_function client-stopped");
    SAH_TRACEZ_INFO(ME, "Fnc \"client-stopped\" returned %d", retval);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

static void odhcp6c_misconfigured(const char* interface) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "Interface", interface);
    retval = amxm_execute_function(NULL, MOD_CORE, "client-misconfigured", &data, &ret);
    when_false_trace(retval == 0, exit, ERROR, "Failed to execute amxm_execute_function client-misconfigured");
    SAH_TRACEZ_INFO(ME, "Fnc \"client-misconfigured\" returned %d", retval);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

static void odhcp6c_stop_handler(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    odhcp6c_t* odhcp6c = (odhcp6c_t*) priv;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "The stop odhcp handler has been triggered");

    when_null_trace(odhcp6c, exit, ERROR, "Could not find the odhcp struct in the priv data");
    when_null_trace(odhcp6c->interface, exit, ERROR, "Could not find the odhcp interface");

    retval = amxp_slot_disconnect(amxp_subproc_get_sigmngr(odhcp6c->subproc), "stop", odhcp6c_stop_handler);
    when_false_trace(retval == 0, exit, ERROR, "Failed to disconnect stop handler");

    if(GET_INT32(data, "ExitCode") != 0) {
        SAH_TRACEZ_ERROR(ME, "ODHCP closed with an exit error %d", GET_INT32(data, "ExitCode"));
        odhcp6c_misconfigured(odhcp6c->interface);
    } else {
        odhcp6c_client_stopped(odhcp6c->interface);
    }
    odhcp6c->state = ODHCP_STOPPED;
    update_state(odhcp6c);
exit:
    return;
}

static void odhcp6c_cleanup_process_cb(odhcp6c_t* odhcp6c) {
    amxc_var_t data_var;
    amxc_var_t ret;
    int retval = -1;

    amxc_var_init(&data_var);
    amxc_var_init(&ret);

    when_null_trace(odhcp6c, exit, ERROR, "Bad odhcp input parameter");
    when_null_trace(odhcp6c->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false(amxp_subproc_is_running(odhcp6c->subproc), exit);

    retval = amxp_slot_disconnect(amxp_subproc_get_sigmngr(odhcp6c->subproc), "stop", odhcp6c_stop_handler);
    when_false_trace(retval == 0, exit, ERROR, "Failed to disconnect stop handler");

    odhcp6c_stop(odhcp6c);
    amxc_var_clean(odhcp6c->used_args);

    amxc_var_set_type(&data_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &data_var, "ExitCode", 0);
    odhcp6c_stop_handler(NULL, &data_var, (void*) odhcp6c);
exit:
    amxc_var_clean(&data_var);
    amxc_var_clean(&ret);
}

static int odhcp6c_stop(odhcp6c_t* odhcp6c) {
    int retval = -1;

    when_null_trace(odhcp6c, exit, ERROR, "Bad odhcp input parameter");
    when_null_trace(odhcp6c->subproc, exit, ERROR, "Could not find the odhcp subproc");
    when_false_trace(amxp_subproc_is_running(odhcp6c->subproc), exit, ERROR, "trying to stop the subproc, but the subproc is not running");

    odhcp6c->state = ODHCP_STOPPING;

    retval = amxp_subproc_kill(odhcp6c->subproc, SIGTERM);
    when_false_trace(retval == 0, exit, ERROR, "Failed to kill the subproc with SIGTERM %s (%d)", strerror(errno), errno);
    retval = amxp_subproc_wait(odhcp6c->subproc, GRACEFUL_STOP_TIMEOUT);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "dhcpv6 client (odhcp6c) is still running after %d ms, sending SIGKILL", GRACEFUL_STOP_TIMEOUT);
        retval = amxp_subproc_kill(odhcp6c->subproc, SIGKILL);
        when_false_trace(retval == 0, exit, ERROR, "Failed to kill the subproc with SIGKILL %s (%d)", strerror(errno), errno);
    }
    SAH_TRACEZ_INFO(ME, "Terminate odhcp6c[%d]", odhcp6c->subproc->pid);
exit:
    return retval;
}

/**
   @brief
   Function called by script

   @param function_name function name
   @param args { phys_interf = "eth0",  action = see odhcp6c_actions.c}
   @param ret 0 (success) or -1 (error occured)
   @return Always 0
 */
int odhcp6c_script(UNUSED const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret) {
    const char* ifname = NULL;
    const char* action = NULL;
    int retval = -1;
    odhcp6c_t* odhcp6c = NULL;

    when_null_trace(args, exit, ERROR, "Bad input args parameter");
    action = GET_CHAR(args, "action");
    when_str_empty_trace(action, exit, ERROR, "Could not find the action param in the args");
    ifname = GET_CHAR(args, "phys_interf");
    when_str_empty_trace(ifname, exit, ERROR, "Could not find the phys_interf param in the args");
    odhcp6c = odhcp6c_list_find_intf(ifname);
    when_null_trace(odhcp6c, exit, ERROR, "Could not find the odhcp6c struct for interface %s", ifname);

    retval = actions_handler(odhcp6c->interface, action, args);
    when_false_trace(retval == 0, exit, ERROR, "Failed to handle the action %s", action);

    if(strcmp(action, "started") == 0) {
        odhcp6c->state = ODHCP_STARTED;
        update_state(odhcp6c);
    }
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

/**
   @brief
   Renew lease of one DHCPv6 client

   @param function_name function name
   @param args { Interface = "IP.Interface.2." }
   @param ret 0 (success) or -1 (error occured)
   @return Always 0
 */
int odhcp6c_renew(UNUSED const char* function_name,
                  amxc_var_t* args,
                  amxc_var_t* ret) {
    const char* interface = GET_CHAR(args, "Interface");
    odhcp6c_t* odhcp6c = odhcp6c_list_find(interface);
    int retval = -1;

    when_null_trace(odhcp6c, exit, ERROR, "odhcp6c info for %s not found", interface);
    when_null_trace(odhcp6c->subproc, exit, ERROR, "subproc info for %s not found", interface);

    retval = amxp_subproc_kill(odhcp6c->subproc, SIGUSR1);
    when_false_trace(retval == 0, exit, ERROR, "Failed to send the SIGUSR1 signal to odhcp");
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

static int odhcp6c_call(const char* interface, const char* method, amxc_var_t* args, amxc_var_t* ret) {
    int retval = -1;
    amxc_string_t object_name;
    amxb_bus_ctx_t* odhcp6c_ctx = NULL;
    const char* object_name_str = NULL;

    amxc_string_init(&object_name, 0);
    when_str_empty_trace(interface, exit, ERROR, "Interface empty");

    amxc_string_setf(&object_name, "odhcp6c.%s", interface);
    object_name_str = amxc_string_get(&object_name, 0);
    odhcp6c_ctx = amxb_be_who_has(object_name_str);
    when_null_trace(odhcp6c_ctx, exit, ERROR, "Failed to find bus context for object %s", object_name_str);

    retval = amxb_call(odhcp6c_ctx, object_name_str, method, args, ret, 1);
    when_failed_trace(retval, exit, ERROR, "Failed to execute '%s' on object %s : %d", method, object_name_str, retval);
exit:
    amxc_string_clean(&object_name);
    return retval;
}

int odhcp6c_get_stats(UNUSED const char* function_name,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    return odhcp6c_call(GET_CHAR(args, "ifname"), "get_statistics", NULL, ret);
}

static void update_args(odhcp6c_t* odhcp6c) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t req_opt_csv;
    amxc_var_t* req_opt;
    amxc_var_t* send_opt;
    amxc_array_t sent_opts;
    const char* ifname = GET_CHAR(odhcp6c->latest_args, "ifname");
    uint32_t dscp = GET_UINT32(odhcp6c->latest_args, "dscp");
    uint32_t sol_max_rt = GET_UINT32(odhcp6c->latest_args, "sol_max_rt");
    bool request_addresses = GET_BOOL(odhcp6c->latest_args, "RequestAddresses");
    bool request_prefixes = GET_BOOL(odhcp6c->latest_args, "RequestPrefixes");
    const char* request_option = GET_CHAR(odhcp6c->latest_args, "RequestedOptions");

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&req_opt_csv);
    amxc_array_init(&sent_opts, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, &args, "dscp", dscp);
    if(sol_max_rt != 0) {
        amxc_var_add_key(uint32_t, &args, "sol_timeout", sol_max_rt);
    }
    amxc_var_add_key(cstring_t, &args, "req_addresses", request_addresses ? "try" : "none");
    amxc_var_add_key(uint32_t, &args, "req_prefixes", request_prefixes ? 0 : NO_IA_PD);
    amxc_var_add_key(bool, &args, "opt_strict", true);

    req_opt = amxc_var_add_key(amxc_llist_t, &args, "opt_requested", NULL);
    if(!str_empty(request_option)) {
        amxc_var_set(csv_string_t, &req_opt_csv, request_option);
        amxc_var_cast(&req_opt_csv, AMXC_VAR_ID_LIST);
        amxc_var_for_each(var, &req_opt_csv) {
            amxc_var_add(uint32_t, req_opt, GET_UINT32(var, NULL));
        }
    }

    odhcp6c_set_sent_options(GET_ARG(odhcp6c->latest_args, "SentOption"), &sent_opts);
    send_opt = amxc_var_add_key(amxc_llist_t, &args, "opt_send", NULL);
    for(size_t i = 0; i < amxc_array_size(&sent_opts); i++) {
        amxc_var_add(cstring_t, send_opt, amxc_array_get_data_at(&sent_opts, i));
    }

    retval = odhcp6c_call(ifname, "reconfigure_dhcp", &args, &ret);
    when_failed_trace(retval, exit, ERROR, "Failed to reconfigure DHCP for %s with error %d", ifname, retval);

    amxc_var_copy(odhcp6c->used_args, odhcp6c->latest_args);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&req_opt_csv);
    amxc_array_clean(&sent_opts, odhcp6c_free_array_item);
}

static void update_state(odhcp6c_t* odhcp6c) {

    when_null_trace(odhcp6c, exit, ERROR, "Bad odhcp6c input parameter");

    if((odhcp6c->state == ODHCP_STARTED) || (odhcp6c->state == ODHCP_STOPPED)) {
        if((amxc_var_get_first(odhcp6c->used_args) != NULL) && (amxc_var_get_first(odhcp6c->latest_args) != NULL)) {
            int cmp = -1;
            int ret = amxc_var_compare(odhcp6c->used_args, odhcp6c->latest_args, &cmp);
            when_failed_trace(ret, exit, ERROR, "Failed to compare two variants");
            when_false_trace(cmp != 0, exit, INFO, "Changed to the correct state %d", odhcp6c->state);
        }

        //Check if the process is in process
        if(amxc_var_get_first(odhcp6c->latest_args) == NULL) {
            //Should be stopped
            odhcp6c_stop(odhcp6c);
            amxc_var_clean(odhcp6c->used_args);
        } else {
            //Should be started
            if((amxc_var_get_first(odhcp6c->used_args) == NULL) || (odhcp6c->state == ODHCP_STOPPED)) {
                amxc_var_copy(odhcp6c->used_args, odhcp6c->latest_args);
                odhcp6c_start(odhcp6c, odhcp6c->used_args);
            } else {
                //Started, but not with the most recent arguments
                update_args(odhcp6c);
            }
        }
    }
exit:
    return;
}

/**
   @brief
   Start a DHCPv6 client

   @param function_name function name
   @param args { Interface = "IP.Interface.2", ifname = "eth0",
                 SentOption [ {Tag = 111,  Value = "" },...],
                 RequestedOptions = "" }
   @return Always 0
 */
int odhcp6c_set_state_start(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    odhcp6c_t* odhcp6c = NULL;
    const char* interface = NULL;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Got a signal to start the odhcp process");

    when_null_trace(args, exit, ERROR, "Bad args input parameter given");
    interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "bad input value 'interface'");

    odhcp6c = odhcp6c_list_find(interface);
    if(odhcp6c == NULL) {
        odhcp6c = odhcp6c_list_add(args);
    }
    when_null_trace(odhcp6c, exit, ERROR, "Failed to create a odhcp6c info struct");
    amxc_var_copy(odhcp6c->latest_args, args);
    update_state(odhcp6c);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

/**
   @brief
   Stop DHCPv6 client

   @param function_name function name
   @param args { Interface = "IP.Interface.2." }
   @param ret status value, 0 if ok
   @return 0 if success
 */
int odhcp6c_set_state_stop(UNUSED const char* function_name,
                           amxc_var_t* args,
                           amxc_var_t* ret) {
    const char* interface = NULL;
    odhcp6c_t* odhcp6c = NULL;
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Got a signal to stop the odhcp process");

    when_null_trace(args, exit, ERROR, "Bad args input parameter given");
    interface = GET_CHAR(args, "Interface");
    when_str_empty_trace(interface, exit, ERROR, "Bad input value 'interface'");

    odhcp6c = odhcp6c_list_find(interface);
    when_null_trace(odhcp6c, exit, ERROR, "Could not find the odhcp struct for the interface %s", interface);
    amxc_var_clean(odhcp6c->latest_args);
    update_state(odhcp6c);
    retval = 0;
exit:
    amxc_var_set(int32_t, ret, retval);
    return 0;
}

static AMXM_CONSTRUCTOR mod_odhcp6c_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Start %s", "mod-odhcp6c");

    odhcp6c_list_init();

    amxm_module_register(&mod, so, MOD_ODHCP6C);
    amxm_module_add_function(mod, "start-dhcpv6c", odhcp6c_set_state_start);
    amxm_module_add_function(mod, "stop-dhcpv6c", odhcp6c_set_state_stop);
    amxm_module_add_function(mod, "odhcp6c-script", odhcp6c_script);
    amxm_module_add_function(mod, "renew-dhcpv6c", odhcp6c_renew);
    amxm_module_add_function(mod, "get-statistics", odhcp6c_get_stats);

    return 0;
}

static AMXM_DESTRUCTOR mod_odhcp6c_stop(void) {
    odhcp6c_list_all(odhcp6c_cleanup_process_cb);
    odhcp6c_list_delete_all();

    return 0;
}
