# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.6.0 - 2024-12-03(17:32:47 +0000)

### Other

- - [tr181-dhcpv6-client] client statistics
- Statistics integrated with ubus

## Release v1.5.11 - 2024-10-03(08:39:29 +0000)

### Other

- [dhcpv6 client] DSCP value in dhcpv6 packet

## Release v1.5.10 - 2024-09-10(08:13:43 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v1.5.9 - 2024-09-09(08:27:33 +0000)

### Other

- - [DHCPv6Client] The HGW shall support Config datamodel

## Release v1.5.8 - 2024-07-29(06:08:02 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v1.5.7 - 2024-07-18(15:36:05 +0000)

### Other

- - [Prpl][CI][tr181-device] Plugins exits on start in reboot scenario (amxs synchronizaiton failed)

## Release v1.5.6 - 2024-07-08(06:56:16 +0000)

## Release v1.5.5 - 2024-06-21(08:43:34 +0000)

### Other

- [CHR2fA][IPV6]DHCPv6 solicit packets interval exceeds expected maximum

## Release v1.5.4 - 2024-06-17(10:28:27 +0000)

### Other

- - amx plugin should not run as root user

## Release v1.5.3 - 2024-06-12(08:16:14 +0000)

### Other

- create odl configuration based on bridges

## Release v1.5.2 - 2024-06-06(12:34:24 +0000)

### Other

- add support for creating new bridge interfaces

## Release v1.5.1 - 2024-05-28(14:37:31 +0000)

### Other

- - CRSP shall support Device.Services.X_PRPL-COM_DeviceConfig.IPv6. TR181 data model parameters - only DM

## Release v1.5.0 - 2024-05-22(07:19:11 +0000)

### New

- [Time] Synchronize the LocalTimeZone using the DHCPv6 option 41

## Release v1.4.2 - 2024-04-10(10:05:14 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.4.1 - 2024-04-08(14:35:17 +0000)

### Fixes

- [DHCPv4Client] [API] Provide a Renew and Release Implementation

## Release v1.4.0 - 2024-03-23(13:10:21 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.3.6 - 2024-03-16(17:12:29 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.3.5 - 2024-03-11(08:20:29 +0000)

### Fixes

- CLONE - [IPv6] After deactivating IPv6, the DUT does not send a DHCPv6 Release message.
- [PRPL CHR2][IPv6]Wrong ipv6 behavior

## Release v1.3.4 - 2024-02-23(09:45:54 +0000)

### Fixes

- [TR181-DHCPv6Client] When neither IA_NA or IA_PD is requested, the DHCPv6Client stays in Error_Misconfigured

## Release v1.3.3 - 2024-02-15(10:56:43 +0000)

### Fixes

- [TR181-DHCPv6Client] No IA_PD is requested despite plugin being configured to do so

## Release v1.3.2 - 2023-12-12(11:34:59 +0000)

### Fixes

- [Repeater Config][IPv6] Implement a proper IPv6 config of the repeater

## Release v1.3.1 - 2023-12-06(11:09:14 +0000)

### Fixes

- [Guest Bridge] Add guest support on AP, using ethernet backhaul.

## Release v1.3.0 - 2023-12-05(15:47:11 +0000)

### New

- [DHCPv6Client] [API] Provide a Renew and Release API's

## Release v1.2.1 - 2023-11-17(14:47:07 +0000)

### Fixes

- [TR181-DHCPv6Client] DHCPv6Client MOP tests are failing

## Release v1.2.0 - 2023-11-09(15:49:02 +0000)

### New

- [IPv6][RouterAdvertisement] M O flag behaviour

## Release v1.1.2 - 2023-10-13(14:04:03 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.1.1 - 2023-10-12(11:27:45 +0000)

### Fixes

- [TR181-DHCPv6Client] Received prefix lifetimes are 10 seconds off

## Release v1.1.0 - 2023-10-11(12:37:17 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v1.0.0 - 2023-08-10(07:58:34 +0000)

### Breaking

- [DHCPv6][CWMP versus USP DM] Add Renew parameter

## Release v0.13.5 - 2023-06-30(15:24:18 +0000)

### Fixes

- [tr181-dhcpv6client] update tr181-dhcpv6client to use amx modules instead of amxb_call

## Release v0.13.4 - 2023-05-11(10:16:55 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.13.3 - 2023-05-04(10:02:57 +0000)

### Fixes

- [tr181-dhcpv6client] Filling in the Client DUID sometimes crashes the plugin

## Release v0.13.2 - 2023-04-20(13:12:57 +0000)

### Fixes

- service protocol should be integers

## Release v0.13.1 - 2023-04-17(12:05:02 +0000)

### Fixes

- [dhcpv6-client] DUID is sometime empty/removed in DM

## Release v0.13.0 - 2023-04-04(10:42:33 +0000)

### New

- Create an event when a renew is received

## Release v0.12.0 - 2023-03-28(12:10:17 +0000)

### New

- [DHCPv6 client] Client does not go in error when interface is down

## Release v0.11.0 - 2023-03-13(15:55:18 +0000)

### Fixes

- [DHCPv6] Split client and server plugin

## Release v0.10.1 - 2023-03-09(09:17:19 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.10.0 - 2023-02-16(11:48:14 +0000)

### New

- [dhcpv6client] Implement RequestAddresses and RequestPrefixes parameters

## Release v0.9.5 - 2023-01-26(07:54:19 +0000)

### Changes

- update DNS data model with hosts from DHCP server pool

## Release v0.9.4 - 2023-01-09(10:20:08 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v0.9.3 - 2022-12-09(09:20:46 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.9.2 - 2022-12-05(12:17:22 +0000)

### Fixes

- [netmodel][DHCPv6Client][getDHCPOption] DHCPv6 option 24 is wrongly parsed

## Release v0.9.1 - 2022-11-29(12:29:27 +0000)

### Fixes

- [tr181][dhcpv6c] lifetime seems to be incorrect

## Release v0.9.0 - 2022-11-24(14:40:56 +0000)

### New

- [tr181][dhcpv6client] store option 23 OPTION_DNS_SERVERS

## Release v0.8.3 - 2022-11-24(10:23:01 +0000)

### Fixes

- [TR181-DHCPv6Client] add/delete firewall rule only when enable changes

## Release v0.8.2 - 2022-11-15(18:15:27 +0000)

### Fixes

- [DHCPv6client] DHCPv6 goes to "Error_Misconfigured" after toggling IP wan enabled

## Release v0.8.1 - 2022-11-07(12:17:45 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v0.8.0 - 2022-10-06(07:26:23 +0000)

### New

- support option AFTR

## Release v0.7.10 - 2022-07-29(07:37:56 +0000)

### Fixes

- dhcpv servers fails to create cpe-dhcpvXs-* firewall services on firstboot

## Release v0.7.9 - 2022-07-28(12:08:18 +0000)

### Fixes

- DHCP v4/v6 managers started alongside clients

## Release v0.7.8 - 2022-07-12(14:26:44 +0000)

## Release v0.7.7 - 2022-06-30(11:41:02 +0000)

### Fixes

- [dhcpv6client] Not starting (Error_Misconfigured)

## Release v0.7.6 - 2022-06-23(09:59:36 +0000)

### Fixes

- [tr181-dhcpv4client] fials to load datamodel at startup

## Release v0.7.5 - 2022-06-14(12:32:03 +0000)

### Fixes

- [TR181 DHCPv6Client] kill previously launched odhcp6c when fnc start-dhcpv6c is called

## Release v0.7.4 - 2022-04-14(07:28:57 +0000)

### Changes

- Make the Interface path prefixed

## Release v0.7.3 - 2022-03-24(10:47:03 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.7.2 - 2022-03-07(14:24:33 +0000)

### Changes

- Interface parameter change is not taken into account, use of netmodel

## Release v0.7.1 - 2022-02-25(11:53:23 +0000)

### Other

- Enable core dumps by default

## Release v0.7.0 - 2022-01-28(11:23:23 +0000)

### New

- ADDRESSES, PREFIXES environment vars from odhcp6c to multi instance ReceivedOptions

## Release v0.6.1 - 2022-01-12(15:59:00 +0000)

### Fixes

- test coverage in unit test

## Release v0.6.0 - 2022-01-11(14:32:16 +0000)

### New

- Create DHCPv6 Client mapping in NetModel

## Release v0.5.0 - 2022-01-10(08:07:53 +0000)

### New

- Retrieve and RequestedOptions

## Release v0.4.0 - 2021-12-22(22:29:28 +0000)

### New

- Support SentOption

## Release v0.3.0 - 2021-11-02(11:30:03 +0000)

### New

- Generate event when updating Status, SourceAddress of server in datamodel

## Release v0.2.0 - 2021-10-25(15:53:20 +0000)

### New

- [TR181-DHCPv6 Client] use common datamodel with DHCPv6 server

## Release v0.1.1 - 2021-09-17(10:04:58 +0000)

### Other

- Opensource component
- Update dependencies in .gitlab-ci.yml file

## Release v0.1.0 - 2021-09-15(07:25:16 +0000)

### New

- Creation of amx plugin

